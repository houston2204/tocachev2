<?php

class GiroController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		
		$hoy = (time()-18000)-((time()-18000)%86400);
		$maniana = $hoy + 86400;
		$giros = DB::table('contratos')->where('servicios_id', '=', 4)->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {

				return View::make('giro.inicio')->with('giros', $giros)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				
				return View::make('giro.inicio')->with('giros', $giros)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}
	}

	public function create()
	{
		//
	}

	public function store()
	{
		function numtoletras($xcifra){

		    $xarray = array(0 => "Cero",
		        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		    );

		    $xcifra = trim($xcifra);
		    $xlength = strlen($xcifra);
		    $xpos_punto = strpos($xcifra, ".");
		    $xaux_int = $xcifra;
		    $xdecimales = "00";
		    if (!($xpos_punto === false)) {
		        if ($xpos_punto == 0) {
		            $xcifra = "0" . $xcifra;
		            $xpos_punto = strpos($xcifra, ".");
		        }
		        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		    }

		    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT);
		    $xcadena = "";
		    for ($xz = 0; $xz < 3; $xz++) {
		        $xaux = substr($XAUX, $xz * 6, 6);
		        $xi = 0;
		        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		        $xexit = true; // bandera para controlar el ciclo del While
		        while ($xexit) {
		            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
		                break; // termina el ciclo
		            }

		            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
		            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
		            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
		                switch ($xy) {
		                    case 1: // checa las centenas
		                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
		                            
		                        } else {
		                            $key = (int) substr($xaux, 0, 3);
		                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
		                                $xseek = $xarray[$key];
		                                $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
		                                if (substr($xaux, 0, 3) == 100)
		                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
		                            }
		                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
		                                $key = (int) substr($xaux, 0, 1) * 100;
		                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
		                                $xcadena = " " . $xcadena . " " . $xseek;
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 0, 3) < 100)
		                        break;
		                    case 2: // checa las decenas (con la misma lógica que las centenas)
		                        if (substr($xaux, 1, 2) < 10) {
		                            
		                        } else {
		                            $key = (int) substr($xaux, 1, 2);
		                            if (TRUE === array_key_exists($key, $xarray)) {
		                                $xseek = $xarray[$key];
		                                $xsub = subfijo($xaux);
		                                if (substr($xaux, 1, 2) == 20)
		                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3;
		                            }
		                            else {
		                                $key = (int) substr($xaux, 1, 1) * 10;
		                                $xseek = $xarray[$key];
		                                if (20 == substr($xaux, 1, 1) * 10)
		                                    $xcadena = " " . $xcadena . " " . $xseek;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 1, 2) < 10)
		                        break;
		                    case 3: // checa las unidades
		                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
		                            
		                        } else {
		                            $key = (int) substr($xaux, 2, 1);
		                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
		                            $xsub = subfijo($xaux);
		                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                        } // ENDIF (substr($xaux, 2, 1) < 1)
		                        break;
		                } // END SWITCH
		            } // END FOR
		            $xi = $xi + 3;
		        } // ENDDO

		        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
		            $xcadena.= " DE";

		        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
		            $xcadena.= " DE";

		        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		        if (trim($xaux) != "") {
		            switch ($xz) {
		                case 0:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN BILLON ";
		                    else
		                        $xcadena.= " BILLONES ";
		                    break;
		                case 1:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN MILLON ";
		                    else
		                        $xcadena.= " MILLONES ";
		                    break;
		                case 2:
		                    if ($xcifra < 1) {
		                        $xcadena = "CERO NUEVOS SOLES CON $xdecimales/100 CENTIMOS";
		                    }
		                    if ($xcifra >= 1 && $xcifra < 2) {
		                        $xcadena = "UN NUEVO SOL CON $xdecimales/100 CENTIMOS";
		                    }
		                    if ($xcifra >= 2) {
		                        $xcadena.= " NUEVOS SOLES CON $xdecimales/100 CENTIMOS"; //
		                    }
		                    break;
		            } // endswitch ($xz)
		        } // ENDIF (trim($xaux) != "")
		        // ------------------      en este caso, para México se usa esta leyenda     ----------------
		        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		    }
		    return trim($xcadena);
		}

		function subfijo($xx){

		    $xx = trim($xx);
		    $xstrlen = strlen($xx);
		    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		        $xsub = "";

		    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		        $xsub = "MIL";

		    return $xsub;
		}

		function guardarPersona($dni, $nombre, $apellidos){

			$persona = new Persona;
			$persona->dni = $dni;
			$persona->nombre = $nombre;
			$persona->apellidos = $apellidos;
			$persona->save();

			return $persona;
		}

		function guardarContrato($costo, $usuario, $ruc, $cliente, $receptor, $destino, $descripcion){

			$contrato = new Contrato;
			$contrato->servicios_id = 4;
			$contrato->costo = $costo;
			$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
			$contrato->usuario = $usuario;
			$contrato->ruc = $ruc;
			$contrato->cliente = $cliente->id;
			$contrato->receptor = $receptor->id;
			$contrato->salida = "AGENCIA ".Agencia::find(Auth::user()->agencias_id)->nombre;
			$contrato->destino = $destino;
			$contrato->descripcion = $descripcion;
			$contrato->estado = 0;
			$contrato->agencia = Auth::user()->agencias_id;
			$contrato->save();

			return $contrato;
		}

		function guardarContrato2($costo, $cliente, $receptor, $destino, $descripcion){

			$contrato = new Contrato;
			$contrato->servicios_id = 4;
			$contrato->costo = $costo;
			$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
			$contrato->cliente = $cliente->id;
			$contrato->receptor = $receptor->id;
			$contrato->salida = "AGENCIA ".Agencia::find(Auth::user()->agencias_id)->nombre;
			$contrato->destino = $destino;
			$contrato->descripcion = $descripcion;
			$contrato->estado = 0;
			$contrato->agencia = Auth::user()->agencias_id;
			$contrato->save();

			return $contrato;
		}

		function actualizarCierre($costo, $monto){

			$cierre = Cierre::find(Auth::user()->cierre);
			$cierre->total += $costo + $monto;
			$cierre->save();
		}

		$usuario = strtoupper(Input::get('usuario'));
		$ruc = Input::get('ruc');
		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$destinatario = Persona::where('dni', '=', Input::get('dnidestinatario'))->first();
		$destino = strtoupper(Input::get('destino'));
		$descripcion = substr(Input::get('descripcion'), 4);
		$costo = substr(Input::get('costo'), 4);

		if ($usuario != null && $ruc != null) {
			
			if ($remitente != null) {
				
				if($destinatario != null){

					$encomienda = guardarContrato($costo, $usuario, $ruc, $remitente, $destinatario, $destino, $descripcion);
				}else{

					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = guardarContrato($costo, $usuario, $ruc, $remitente, $destinatario, $destino, $descripcion);
				}
			}else{

				if ($destinatario != null) {
					
					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$encomienda = guardarContrato($costo, $usuario, $ruc, $remitente, $destinatario, $destino, $descripcion);
				}else{

					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = guardarContrato($costo, $usuario, $ruc, $remitente, $destinatario, $destino, $descripcion);
				}
			}
		}else{
			if ($remitente != null) {
				
				if($destinatario != null){

					$encomienda = guardarContrato2($costo, $remitente, $destinatario, $destino, $descripcion);
				}else{

					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = guardarContrato2($costo, $remitente, $destinatario, $destino, $descripcion);
				}
			}else{

				if ($destinatario != null) {
					
					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$encomienda = guardarContrato2($costo, $remitente, $destinatario, $destino, $descripcion);
				}else{

					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = guardarContrato2($costo, $remitente, $destinatario, $destino, $descripcion);
				}
			}
		}

		actualizarCierre($costo, $descripcion);
		$letras = numtoletras($encomienda->costo);

		return View::make('giro.recibo')->with('encomienda', $encomienda)->with('letras', $letras);
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$encomienda = Contrato::find($id);
		return View::make('giro.mostrar')->with('encomienda', $encomienda)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$encomienda = Contrato::find($id);
		$remitente = Persona::find($encomienda->cliente);
		$destinatario = Persona::find($encomienda->receptor);

		return View::make('giro.modificarremitente')->with('encomienda', $encomienda)->with('remitente', $remitente)
		->with('destinatario', $destinatario)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function update($id)
	{
		function actualizarCierre($difcosto, $difmonto){

			$cierre = Cierre::find(Auth::user()->cierre);
			$cierre->total += $difcosto + $difmonto;
			$cierre->save();
		}

		function guardarPersona($dni, $nombre, $apellidos){

			$persona = new Persona;
			$persona->dni = $dni;
			$persona->nombre = $nombre;
			$persona->apellidos = $apellidos;
			$persona->save();

			return $persona;
		}

		function actualizarPersona($id, $dni, $nombre, $apellidos){

			$persona = Persona::find($id);
			$persona->nombre = $nombre;
			$persona->apellidos = $apellidos;
			$persona->save();

			return $persona;
		}

		function guardarContrato($costo, $usuario, $ruc, $cliente, $receptor, $destino, $descripcion){

			$contrato = new Contrato;
			$contrato->servicios_id = 3;
			$contrato->costo = $costo;
			$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
			$contrato->usuario = $usuario;
			$contrato->ruc = $ruc;
			$contrato->cliente = $cliente->id;
			$contrato->receptor = $receptor->id;
			$contrato->salida = "AGENCIA ".Agencia::find(Auth::user()->agencias_id)->nombre;
			$contrato->destino = $destino;
			$contrato->descripcion = $descripcion;
			$contrato->estado = 1;
			$contrato->save();

			return $contrato;
		}

		function actualizarContrato($id, $costo, $usuario, $ruc, $cliente, $receptor, $destino, $descripcion){

			$contrato = Contrato::find($id);
			$contrato->costo = $costo;
			$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
			$contrato->usuario = $usuario;
			$contrato->ruc = $ruc;
			$contrato->cliente = $cliente->id;
			$contrato->receptor = $receptor->id;
			$contrato->salida = "AGENCIA ".Agencia::find(Auth::user()->agencias_id)->nombre;
			$contrato->destino = $destino;
			$contrato->descripcion = $descripcion;
			$contrato->save();

			return $contrato;
		}

		function actualizarContrato2($id, $costo, $cliente, $receptor, $destino, $descripcion){

			$contrato = Contrato::find($id);
			$contrato->costo = $costo;
			$contrato->ruc = null;
			$contrato->usuario = null;
			$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
			$contrato->cliente = $cliente->id;
			$contrato->receptor = $receptor->id;
			$contrato->salida = "AGENCIA ".Agencia::find(Auth::user()->agencias_id)->nombre;
			$contrato->destino = $destino;
			$contrato->descripcion = $descripcion;
			$contrato->save();

			return $contrato;
		}

		$usuario = strtoupper(Input::get('usuario'));
		$ruc = Input::get('ruc');
		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$destinatario = Persona::where('dni', '=', Input::get('dnidestinatario'))->first();
		$destino = strtoupper(Input::get('destino'));
		$descripcion = substr(Input::get('descripcion'), 4);
		$costo = substr(Input::get('costo'), 4);
		$giro = Contrato::find($id);
		$difcosto = substr(Input::get('costo'), 4) - $giro->costo;
		$difmonto = substr(Input::get('descripcion'), 4) - $giro->descripcion;
			
		if ($ruc != null && $usuario !=null) {
			
			if ($remitente != null) {
				
				if($destinatario != null){

					$encomienda = actualizarContrato($id, $costo, $usuario, $ruc, $remitente, $destinatario, $destino, 
						$descripcion);
				}else{

					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = actualizarContrato($id, $costo, $usuario, $ruc, $remitente, $destinatario, $destino, 
						$descripcion);
				}
			}else{

				if ($destinatario != null) {
					
					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$encomienda = actualizarContrato($id, $costo, $usuario, $ruc, $remitente, $destinatario, $destino, 
						$descripcion);
				}else{

					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = actualizarContrato($id, $costo, $usuario, $ruc, $remitente, $destinatario, $destino, 
						$descripcion);
				}
			}
		}else{

			if ($remitente != null) {
				
				if($destinatario != null){

					$encomienda = actualizarContrato2($id, $costo, $remitente, $destinatario, $destino, 
						$descripcion);
				}else{

					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = actualizarContrato2($id, $costo, $remitente, $destinatario, $destino, 
						$descripcion);
				}
			}else{

				if ($destinatario != null) {
					
					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$encomienda = actualizarContrato2($id, $costo, $remitente, $destinatario, $destino, 
						$descripcion);
				}else{

					$remitente = guardarPersona(Input::get('dniremitente'), strtoupper(Input::get('nombreremitente')), 
						strtoupper(Input::get('apellidosremitente')));
					$destinatario = guardarPersona(Input::get('dnidestinatario'), strtoupper(Input::get('nombredestinatario')), 
						strtoupper(Input::get('apellidosdestinatario')));
					$encomienda = actualizarContrato2($id, $costo, $remitente, $destinatario, $destino, 
						$descripcion);
				}
			}
		}

		actualizarCierre($difcosto, $difmonto);

		return View::make('giro.recibo')->with('encomienda', $encomienda);
	}

	public function destroy($id)
	{
		$giro = Contrato::find($id);
		$cierre = Cierre::find(Auth::user()->cierre);

		if ($giro->descripcion < $cierre->total) {
			
			$giro->estado = 2;
			$giro->save();

			$cierre->total -= $giro->descripcion;
			$cierre->save();
			
			return View::make('giro.recibi')->with('giro', $giro);
		}else{

			$mensaje = "NO HAY DINERO SUFICIENTE EN LA CAJA PARA ENTREGAR EL GIRO, SE SUGIERE COMUNICAR AL ADMINISTRADOR
			A CARGO.";

			return Redirect::to('giro')->with('rojo', $mensaje);
		}

	}

}
