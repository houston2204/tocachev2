<?php

class UsuarioController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$usuarios = DB::table('usuarios')->where('id', '!=', Auth::user()->id)
		->where('id', '!=', 1)->get();
		return View::make('usuario.inicio')->with('usuarios', $usuarios)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function create()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		//mostrar el formulario para ingresar los datos del usuario.
		$usuario = new Usuario;
		$persona = new Persona;
		$direccion = new Direccion;
		return View::make('usuario.nuevo')->with('usuario', $usuario)
		->with('persona', $persona)->with('direccion', $direccion)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function store()
	{
		function guardarpersona(){
			$persona = new Persona;
			$direccion = new Direccion;

			$direccion->lugar = strtoupper(Input::get('lugar'));
			$direccion->distrito = strtoupper(Input::get('distrito'));
			$direccion->provincia = strtoupper(Input::get('provincia'));
			$direccion->departamento = strtoupper(Input::get('departamento'));
			$direccion->save();

			$persona->dni = Input::get('dni');
			$persona->nombre = strtoupper(Input::get('nombre'));
			$persona->apellidos = strtoupper(Input::get('apellidos'));
			$persona->telefono = strtoupper(Input::get('telefono'));
			$persona->direcciones_id = $direccion->id;
			$persona->save();
		}

		function guardarusuario($persona){
			$usuario = new Usuario;

			$tipo = Input::get('tipo');
			if($tipo != 1){
				$tipo = 0;
			}

			$usuario->password = Hash::make($persona->dni);
			$usuario->email = $persona->dni;
			$usuario->personas_id = $persona->id;
			$usuario->agencias_id = Input::get('agencia');
			$usuario->tipo = $tipo;
			$usuario->save();
		}

		//verificamos si el nuevo usuario existe como persona
		$persona = Persona::where('dni', '=', Input::get('dni'))->count();
		if($persona){
			//si existe verificamos si es usuario
			$persona = Persona::where('dni', '=', Input::get('dni'))->first();
			$usuario = Usuario::where('personas_id', '=', $persona->id)->count();
			if($usuario){
				//Si la persona existe y ya es usuario se redirecciona al formulario para 
				//editar los datos del usuario con un mensaje de advertencia.
				$usuario = Usuario::where('personas_id', '=', $persona->id)->first();
				$mensaje = $persona->nombre." ".$persona->apellidos." YA ES USUARIO EN ESTA U OTRA AGENCIA, ACTUALICE SUS DATOS
				CORRECTAMENTE.";
				return Redirect::to('usuario/'.$usuario->id.'/edit')->with('naranja', $mensaje);
			}else{
				//Si la persona existe pero no es usurio guardamos los datos de la tabla usuario con el
				//campo 'personas_id' con el valor de $persona->id
				$persona = Persona::where('dni', '=', Input::get('dni'))->first();
				guardarusuario($persona);
				$mensaje = "EL USUARIO ".$persona->nombre." ".$persona->apellidos." FUE AGREGADO CORRECTAMENTE A LA EMPRESA, 
				PUEDE BUSCARLO EN LA LISTA DE USUARIOS.";
				return Redirect::to('usuario')->with('verde', $mensaje);
			}
		}else{
			//si no existe la persona guardamos los datos en la tabla persona y usuario
			guardarpersona();
			$persona = Persona::where('dni', '=', Input::get('dni'))->first();
			guardarusuario($persona);
			$mensaje = "EL USUARIO ".$persona->nombre." ".$persona->apellidos." FUE AGREGADO CORRECTAMENTE A LA EMPRESA, 
			PUEDE BUSCARLO EN LA LISTA DE USUARIOS.";
			return Redirect::to('usuario')->with('verde', $mensaje);
		}
		
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$usuario = Usuario::find($id);
		return View::make('usuario.mostrar')->with('usuario', $usuario)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		
		$usuario = Usuario::find($id);
		$persona = Persona::find($usuario->personas_id);
		$direccion = Direccion::find($persona->direcciones_id);
		
		return View::make('usuario.nuevo')->with('usuario', $usuario)
		->with('persona', $persona)->with('direccion', $direccion)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function update($id)
	{
		$usuario = Usuario::find($id);
		$persona = Persona::find($usuario->personas_id);
		$direccion = Direccion::find($persona->direcciones_id);

		$direccion->lugar = strtoupper(Input::get('lugar'));
		$direccion->distrito = strtoupper(Input::get('distrito'));
		$direccion->provincia = strtoupper(Input::get('provincia'));
		$direccion->departamento = strtoupper(Input::get('departamento'));
		$direccion->save();

		$persona->nombre = strtoupper(Input::get('nombre'));
		$persona->apellidos = strtoupper(Input::get('apellidos'));
		$persona->telefono = Input::get('telefono');
		$persona->direcciones_id = $direccion->id;
		$persona->save();

		$tipo = Input::get('tipo');
		if($tipo != 1){
			$tipo = 0;
		}

		$usuario->email = $persona->dni;
		$usuario->personas_id = $persona->id;
		$usuario->agencias_id = Input::get('agencia');
		$usuario->tipo = $tipo;
		$usuario->save();

		$mensaje = "~LOS DATOS DEL USUARIO ".$persona->nombre." ".$persona->apellidos." FUE ACTUALIZADO CORRECTAMENTE 
		PUEDE VERLO EN LA LISTA DE USUARIOS.";

		return Redirect::to('usuario')->with('verde', $mensaje);
	}

	public function destroy($id)
	{
		$mensaje = "EL USUARIO ".$persona->nombre." ".$persona->apellidos." FUE BORRADO COMO PERSONAL DE LA EMPRESA,
		PUEDE CONFIRMARLO EN LA LISTA DE USUARIOS";
		//primero vemos si la persona no conduce un auto
		$usuario = Usuario::find($id);
		$persona = Persona::find($usuario->personas_id);
		$auto = Auto::where('conductor', '=', $persona->id)->count();
		if ($auto) {
			//si conduce un auto eliminamos a la persona de la tabla usuarios
			$usuario->delete();
			return Redirect::to('usuario')->with('naranja', $mensaje);
		}else{
			//si no conduce un auto, vemos si es dueño de uno
			$auto = Auto::where('duenio', '=', $persona->id)->count();
			if ($auto) {
				//si no conduce un auto pero es dueño de un auto, 
				//eliminamos a la persona de la tabla usuarios
				$usuario->delete();
				return Redirect::to('usuario')->with('naranja', $mensaje);
			}else{
				//si no conduce un auto y no es dueño de un auto, 
				//eliminamos a la persona de la tabla usuarios y la tabla personas
				$usuario->delete();
				$direccion = Direccion::find($persona->direcciones_id);
				$direccion->delete();
				return Redirect::to('usuario')->with('naranja', $mensaje);
			}
		}
	}


}
