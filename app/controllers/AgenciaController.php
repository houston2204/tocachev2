<?php

class AgenciaController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		$agencias = DB::table('agencias')->get();

		return View::make('agencia.inicio')->with('agencias', $agencias)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function create()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$agencia = new Agencia;

		return View::make('agencia.nuevo')->with('agencia', $agencia)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function store()
	{
		$direccion = new Direccion;
		$direccion->lugar = strtoupper(Input::get('lugar'));
		$direccion->distrito = strtoupper(Input::get('distrito'));
		$direccion->provincia = strtoupper(Input::get('provincia'));
		$direccion->departamento = strtoupper(Input::get('departamento'));
		$direccion->save();

		$agencia = new Agencia;
		$agencia->nombre = strtoupper(Input::get('nombre'));
		$agencia->telefono = strtoupper(Input::get('telefono'));
		$agencia->direcciones_id = $direccion->id;
		$agencia->save();

		$mensaje = "LA AGENCIA SE CREO SATISFACTORIAMENTE, AHORA PUEDE BUSCARLO EN LA LISTA DE AGENCIAS.";

		return Redirect::to('agencia')->with('verde', $mensaje);
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		$agencia = Agencia::find($id);

		return View::make('agencia.mostrar')->with('agencia', $agencia)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		$agencia = Agencia::find($id);

		return View::make('agencia.editar')->with('agencia', $agencia)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function update($id)
	{
		$agencia = Agencia::find($id);
		$agencia->nombre = strtoupper(Input::get('nombre'));
		$agencia->telefono = strtoupper(Input::get('telefono'));
		$agencia->save();

		$direccion = Direccion::find($agencia->direcciones_id);
		$direccion->lugar = strtoupper(Input::get('lugar'));
		$direccion->distrito = strtoupper(Input::get('distrito'));
		$direccion->provincia = strtoupper(Input::get('provincia'));
		$direccion->departamento = strtoupper(Input::get('departamento'));
		$direccion->save();

		$mensaje = "ACABA DE ACTUALIZAR LOS DATOS LA AGENCIA ".$agencia->nombre.
		" SATISFACTORIAMENTE, PUEDE VERLO EN LA LISTA DE AGENCIAS.";

		return Redirect::to('agencia')->with('verde', $mensaje);
	}

	public function destroy($id)
	{
		$agencia = Agencia::find($id);
		$direccion = Direccion::find($agencia->direcciones_id);


		if ($agencia->id == Auth::user()->agencias_id) {

			$mensaje = "NO PUEDE BORRAR LA AGENCIA ".$agencia->nombre.
			" YA QUE USTED TRABAJA EN ESTA AGENCIA, PRUEBE DESDE OTRO USUARIO AJENO A ESTA AGENCIA.";

			return Redirect::to('agencia')->with('rojo', $mensaje);
		}else{
			
			$agencia->delete();
			$direccion->delete();
			
			$mensaje = "ACABA DE BORRAR LA AGENCIA ".$agencia->nombre.
			" CON TODOS SUS DATOS, YA NO PODRA ENCONTRARLA EN LA LISTA DE AGENCIAS.";
			
			return Redirect::to('agencia')->with('naranja', $mensaje);
		}


	}


}
