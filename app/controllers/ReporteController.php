<?php

class ReporteController extends \BaseController {

	public function getNomina(){
		
		$dia = date('d-m-Y_his', time()-18000);

		Excel::create('TOCACHE EXPRESS '.$dia, function($documento){

			$documento->sheet('NOMINA DE CONDUCTORES', function($sheet){

				$datos=[];
				array_push($datos, array('NÓMINA DE CONDUCTORES'));
				array_push($datos, array(' '));
				array_push($datos, array('RAZÓN SOCIAL:           TRANSPORTES E INVERSIONES "TOCACHE EXPRESS" SAC'));
				array_push($datos, array('DOMICILIO FISCAL:     JR. JORGE CHAVEZ Nº 1206 - TARAPOTO'));
				array_push($datos, array('RUC Nº:                        20450340031'));
				array_push($datos, array(' '));
				array_push($datos, array(' '));
				array_push($datos, array('CÓDIGO', 'APELLIDOS Y NOMBRES', 'Nº DE DNI', 'Nº LICENCIA', 'CATEGORÍA', 'VENCIMIENTO',
					'DIRECCIÓN'));

				$licencias = DB::table('licencias')->get();
				$filas = 8;
				foreach ($licencias as $licencia) {
					
					$persona = Persona::find($licencia->personas_id);
					$direccion = Direccion::find($persona->direcciones_id);
					array_push($datos, array($licencia->codigo, $persona->apellidos." ".$persona->nombre, $persona->dni,
						$licencia->numero, $licencia->categoria, $licencia->vencimiento, $direccion->lugar." ".
						$direccion->distrito." ".$direccion->provincia." ".$direccion->departamento));
					$filas++;
				}
				$sheet->fromArray($datos, null, 'A1', false, false);
				$sheet->mergeCells('A1:G1');
				$sheet->mergeCells('A3:G3');
				$sheet->mergeCells('A4:G4');
				$sheet->mergeCells('A5:G5');

				$sheet->cells('A1:G1', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'14',
						'bold'		=>	true
					));
					$celdas->setAlignment('center');
				});

				$sheet->cells('A3:G7', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'10',
						'bold'		=>	true
					));
				});

				$sheet->cells('A8:G8', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'11',
						'bold'		=>	true
					));
					$celdas->setAlignment('center');
				});

				$sheet->setWidth(array(

					'A'	=>	'9',
					'B'	=>	'35',
					'C'	=>	'11',
					'D'	=>	'13',
					'E'	=>	'12',
					'F'	=>	'14',
					'G'	=>	'60'
				));

				$sheet->setFontBold(true);
				$sheet->setBorder('A8:G'.$filas, 'thin');
			});
			
			$mensaje = "SE DEBE ESTAR INICIANDO LA DESCARGA DE LA NOMINA DE CONDUCTORES. REVISE SU CARPETA DE DESCARGAS.";
		})->download('xlsx')->with('verde', $mensaje);
	}

	public function getPadron(){
		
		$dia = date('d-m-Y_his', time()-18000);

		Excel::create('TOCACHE EXPRESS '.$dia, function($documento){

			$documento->sheet('PADRÓN VEHICULAR', function($sheet){

				$datos=[];
				array_push($datos, array('PADRÓN DE FLOTA VEHICULAR'));
				array_push($datos, array(' '));
				array_push($datos, array('RAZÓN SOCIAL:           TRANSPORTES E INVERSIONES "TOCACHE EXPRESS" SAC'));
				array_push($datos, array('DOMICILIO FISCAL:     JR. JORGE CHAVEZ Nº 1206 - TARAPOTO'));
				array_push($datos, array('RUC Nº:                        20450340031'));
				array_push($datos, array(' '));
				array_push($datos, array(' '));
				array_push($datos, array('CÓDIGO', 'Nº PLACA', 'MARCA', 'MODELO', 'COLOR', 'Nº DE SOAT',
					'VENCIMIENTO', 'DUEÑO', 'DIRECCIÓN', 'TELEFONO', 'CONDUCTOR'));

				$autos = DB::table('autos')->get();
				$filas = 8;
				foreach ($autos as $auto) {
					
					$seguro = Auto::find($auto->id)->seguro;
					$duenio = Persona::find($auto->duenio);
					$conductor = Persona::find($auto->conductor);
					$licencia = $conductor->licencia;
					$direccion = Direccion::find($duenio->direcciones_id);
					array_push($datos, array($licencia->codigo, $auto->placa, $auto->marca, $auto->modelo, $auto->color,
						$seguro->numero, $seguro->vencimiento, $duenio->nombre." ".$duenio->apellidos, $direccion->lugar." ".
						$direccion->distrito." ".$direccion->provincia." ".$direccion->departamento, $duenio->telefono,
						$conductor->nombre." ".$conductor->apellidos));
					$filas++;
				}
				$sheet->fromArray($datos, null, 'A1', false, false);
				$sheet->mergeCells('A1:K1');
				$sheet->mergeCells('A3:K3');
				$sheet->mergeCells('A4:K4');
				$sheet->mergeCells('A5:K5');

				$sheet->cells('A1:K1', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'14',
						'bold'		=>	true
					));
					$celdas->setAlignment('center');
				});

				$sheet->cells('A3:K7', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'10',
						'bold'		=>	true
					));
				});

				$sheet->cells('A8:K8', function($celdas){

					$celdas->setFont(array(
						'family'	=>	'Arial',
						'size'		=>	'11',
						'bold'		=>	true
					));
					$celdas->setAlignment('center');
				});

				$sheet->setWidth(array(

					'A'	=>	'9',
					'B'	=>	'10',
					'C'	=>	'10',
					'D'	=>	'10',
					'E'	=>	'10',
					'F'	=>	'13',
					'G'	=>	'13',
					'H'	=>	'35',
					'I'	=>	'60',
					'J'	=>	'11',
					'K'	=>	'35',
				));

				$sheet->setFontBold(true);
				$sheet->setBorder('A8:K'.$filas, 'thin');
			});
			
			$mensaje = "SE DEBE ESTAR INICIANDO LA DESCARGA DE LA NOMINA DE CONDUCTORES. REVISE SU CARPETA DE DESCARGAS.";
		})->download('xlsx')->with('verde', $mensaje);
	}

	public function getServicios(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		return View::make('reporte.buscar')->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function postGenerar(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		function fechas($rango, &$inicio, &$fin){

			$inicio = substr($rango, 0, 10);
			$inicio = substr($inicio, 3, 2)."/".substr($inicio, 0, 2)."/".substr($inicio, 6, 4);
			$fin = substr($rango, 13);
			$fin = substr($fin, 3, 2)."/".substr($fin, 0, 2)."/".substr($fin, 6, 4);
		}

		if(Input::get('rango') != null){

			fechas(Input::get('rango'), $inicio, $fin);

			$inicio = strtotime($inicio);
			$fin = strtotime($fin) + 86399;
			$contratos = DB::table('contratos')->where('servicios_id', '=', Input::get('servicio'))->get();

			$servicios = [];
			$i = 0;
			$total = 0;

			foreach ($contratos as $contrato) {
				
				if(strtotime($contrato->created_at) >= $inicio && strtotime($contrato->created_at) < $fin){

					$servicios[$i] = $contrato;
					$i++;
					$total += $contrato->costo;
				}
			}

			if (Input::get('servicio') == 1) {
				
				return View::make('reporte.salidas')->with('servicios', $servicios)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw)->with('total', $total);
			}elseif (Input::get('servicio') == 2) {
				
				return View::make('reporte.pasajes')->with('servicios', $servicios)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw)->with('total', $total);
			}else{
				return Input::get('servicio');
			}
		}else{

			$mensaje = "NO INGRESO UN RANGO DE FECHAS PARA MOSTRAR SU REPORTE. VUELVA A INTENTARLO.";

			return Redirect::to('reporte/servicios')->with('rojo', $mensaje);
		}
	}
}
