<?php

class ContraseniaController extends BaseController{

	public function getIndex(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		
		return View::make('usuario.cambiarcontrasenia')->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
		
	}

	public function postCambiar(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$dni = Input::get('dni');
		$password = Input::get('password');
		$newpassword1 = Input::get('newpassword1');
		$newpassword2 = Input::get('newpassword2');

		if(Auth::attempt(array('email'=>$dni, 'password'=>$password))){

			if($newpassword1 == $newpassword2){

				$usuario = Usuario::find(Auth::user()->id);
				$usuario->password = Hash::make($newpassword1);
				$usuario->save();
				$resultado = "Su contraseña fue cambiado con exito";

				return View::make('usuario.mensajes')->with('resultado', $resultado)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				
			}else{

				$resultado = "Las nuevas contraseñas no coinciden";

				return View::make('usuario.mensajes')->with('resultado', $resultado)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$resultado = "Su contraseña o DNI estan erradas";
			
			return View::make('usuario.mensajes')->with('resultado', $resultado)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
		}
	}
}