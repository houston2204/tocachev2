<?php

class ConductorController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$conductores = DB::table('personas')
		->rightjoin('licencias', 'licencias.personas_id', '=', 'personas.id')
		->leftjoin('autos', 'personas.id', '=', 'autos.conductor')
		->select('personas.id as idpersona', 'personas.dni', 'personas.nombre', 'personas.apellidos', 
			'autos.id as idauto', 'autos.placa', 'licencias.id as idlicencia')
		->get();
		return View::make('conductor.inicio')->with('conductores', $conductores)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function create()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$direccioncond = new Direccion;
		$conductor = new Persona;
		$licencia = new Licencia;
		$direccionduen = new Direccion;
		$duenio = new Persona;
		$auto = new Auto;
		$seguro = new Seguro;

		return View::make('conductor.nuevo')->with('conductor', $conductor)
		->with('direccioncond', $direccioncond)->with('licencia', $licencia)
		->with('auto', $auto)->with('seguro', $seguro)
		->with('duenio', $duenio)->with('direccionduen', $direccionduen)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function store()
	{
		function formatearfecha($fecha){
			$formato = $fecha;
			$dia = substr($formato, 0, 2);
			$mes = substr($formato, 3, 2);
			$anio = substr($formato, 6, 4);
			$formato = $anio."-".$mes."-".$dia;
			return $formato;
		}

		function guardarconductor(){
			$direccion = new Direccion;
			$persona = new Persona;
			$licencia = new Licencia;

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->dni = strtoupper(Input::get('dniconductor'));
			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = Input::get('telefonoconductor');
			$persona->direcciones_id = $direccion->id;
			$persona->save();

			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $persona->id;
			$licencia->save();
		}

		function guardarduenio(){
			$direccion = new Direccion;
			$persona = new Persona;

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->dni = Input::get('dniduenio');
			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->direcciones_id = $direccion->id;
			$persona->save();
		}

		function guardarauto($conductor, $duenio){
			$auto = new Auto;
			$seguro = new Seguro;

			$auto->placa = strtoupper(Input::get('placa'));
			$auto->marca = strtoupper(Input::get('marca'));
			$auto->modelo = strtoupper(Input::get('modelo'));
			$auto->color = strtoupper(Input::get('color'));
			$auto->conductor = $conductor;
			$auto->duenio = $duenio;
			$auto->save();

			$seguro->numero = strtoupper(Input::get('seguro'));
			$seguro->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('vencseguro'))));
			$seguro->autos_id = $auto->id;
			$seguro->save();
		}

		function guardarlicencia($conductor){
			$licencia = new Licencia;

			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $conductor;
			$licencia->save();
		}

		//verificamos si el conductro existe.
		$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->count();
		if($conductor){
			//Si existe el conductor verificamos si conduce un auto
			$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
			$auto = Auto::where('conductor', '=', $conductor->id)->count();
			if ($auto) {
				//si conduce redireccionamos a una página de error
				//El conductor ya maneja un auto
				$mensaje = "EL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
				" YA CONDUCE UN AUTO, SE RECOMIENDA QUE ACTUALICE SUS DATOS SI ES EL CASO.";
				return Redirect::to('conductor/'.$conductor->id.'/edit')->with('naranja', $mensaje);
			}else{
				//Si no conduce un auto verificamos si el auto existe
				$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->count();
				if($auto){
					//Si el auto existe redireccionamos a una pagina de error.
					//el auto ya es manejado por otro.
					$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->first();
					$mensaje = "EL AUTO ".$auto->placa." YA TIENE UN CONDUCTOR, SE RECOMIENDA QUE ACTUALICE 
					LOS DATOS DEL AUTO SI ES EL CASO.";
					return Redirect::to('auto/'.$auto->id.'/edit')->with('naranja', $mensaje);
				}else{
					//si el auto no existe verificamos si el dueño existe
					$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->count();
					if($duenio){
						//si el dueño existe guardamos los datos del conductor y el auto con los datos
						//del dueño ya existente y redireccionamos a la lista de conductores.
						$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
						$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->first();
						guardarlicencia($conductor->id);
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
						" Y EL AUTO QUE CONDUCE FUERON AGREGADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}else{
						//si el dueño no existe guardamos la licencia del conductor, el duenño y el auto 
						//y redireccionamos a la lista de conductores.
						guardarduenio();
						$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
						$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->first();
						guardarlicencia($conductor->id);
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
						" Y EL AUTO QUE CONDUCE FUERON AGREGADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}
				}
			}
		}else{
			//Si el conductor no existe verificamos si el auto existe
			$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->count();
			if($auto){
				//Si el auto existe redireccionamos a una pagina de error.
				//el auto ya es manejado por otro.
				$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->first();
				$mensaje = "EL AUTO ".$auto->placa." YA TIENE UN CONDUCTOR, SE RECOMIENDA QUE ACTUALICE 
				LOS DATOS DEL AUTO SI ES EL CASO.";
				return Redirect::to('auto/'.$auto->id.'/edit')->with('naranja', $mensaje);
			}else{
				//si el auto no existe verificamos si el dueño existe
				$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->count();
				if($duenio){
					//si el dueño existe guardamos los datos del conductor y el auto con los datos
					//del dueño ya existente y redireccionamos a la lista de conductores.
					guardarconductor();
					$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
					$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->first();
					guardarauto($conductor->id, $duenio->id);
					$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
					" Y EL AUTO QUE CONDUCE FUERON AGREGADOS SATISFACTORIAMENTE.";
					return Redirect::to('conductor')->with('verde', $mensaje);
				}else{
					//si el dueño no existe verificamos si el conductor y el dueño son la misma persona.
					if(input::get('dniconductor') == strtoupper(input::get('dniduenio'))){
						//si son la misma persona guardamo al conductor y el auto con los datos del conductor.
						guardarconductor();
						$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
						guardarauto($conductor->id, $conductor->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
						" Y EL AUTO QUE CONDUCE FUERON AGREGADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}else{
						//si no son la misma persona guardamos al conductor, dueño y el auto.
						guardarconductor();
						guardarduenio();
						$conductor = Persona::where('dni', '=', strtoupper(Input::get('dniconductor')))->first();
						$duenio = Persona::where('dni', '=', strtoupper(Input::get('dniduenio')))->first();
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
						" Y EL AUTO QUE CONDUCE FUERON AGREGADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}
				}
			}
		}
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$conductor = Persona::find($id);
		return View::make('conductor.mostrar')->with('conductor', $conductor)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		
		$conductor = Persona::find($id);
		$direccioncond = Direccion::find($conductor->direcciones_id);
		$licencia = $conductor->licencia;
		$auto = $conductor->vehiculo;
		$seguro = $auto->seguro;
		$duenio = Persona::find($auto->duenio);
		$direccionduen = Direccion::find($duenio->direcciones_id);

		return View::make('conductor.nuevo')->with('conductor', $conductor)
		->with('direccioncond', $direccioncond)->with('licencia', $licencia)
		->with('auto', $auto)->with('seguro', $seguro)
		->with('duenio', $duenio)->with('direccionduen', $direccionduen)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function update($id)
	{
		function formatearfecha($fecha){
			$formato = $fecha;
			$dia = substr($formato, 0, 2);
			$mes = substr($formato, 3, 2);
			$anio = substr($formato, 6, 4);
			$formato = $anio."-".$mes."-".$dia;
			return $formato;
		}

		function actualizarconductor($id){

			$persona = Persona::find($id);
			$direccion = Direccion::find($persona->direcciones_id);
			$licencia = $persona->licencia;

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = Input::get('telefonoconductor');
			$persona->save();

			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $persona->id;
			$licencia->save();
		}

		function actualizarduenio(){
			$persona = Persona::where('dni', '=', Input::get('dniduenio'))->first();
			$direccion = Direccion::find($persona->direcciones_id);

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->save();
		}

		function nuevoduenio(){
			$persona = new Persona;
			$direccion = new Direccion;

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->dni = Input::get('dniduenio');
			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->save();
		}

		function actualizarauto($conductor, $duenio){
			$auto = Auto::where('placa', '=', input::get('placa'))->first();
			$seguro = Seguro::where('autos_id', '=', $auto->id)->first();

			$auto->marca = strtoupper(Input::get('marca'));
			$auto->modelo = strtoupper(Input::get('modelo'));
			$auto->color = strtoupper(Input::get('color'));
			$auto->conductor = $conductor;
			$auto->duenio = $duenio;
			$auto->save();

			$seguro->numero = strtoupper(Input::get('seguro'));
			$seguro->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('vencseguro'))));
			$seguro->save();
		}

		function nuevoauto($conductor, $duenio){
			$auto = new Auto;
			$seguro = new Seguro;

			$auto->placa = strtoupper(Input::get('placa'));
			$auto->marca = strtoupper(Input::get('marca'));
			$auto->modelo = strtoupper(Input::get('modelo'));
			$auto->color = strtoupper(Input::get('color'));
			$auto->conductor = $conductor;
			$auto->duenio = $duenio;
			$auto->save();

			$seguro->numero = strtoupper(Input::get('seguro'));
			$seguro->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('vencseguro'))));
			$seguro->autos_id = $auto->id;
			$seguro->save();
		}

		//cambiamos datos del conductor
		//Vemos si se va a guardar el mismo auto
		$auto = Auto::where('conductor', '=', $id)->first();
		if($auto->placa == strtoupper(Input::get('placa'))){
			//el conductor va a continuar con el mismo auto,
			//vemos que no esté cambiando de dueño
			$duenio = Persona::find($auto->duenio);
			if($duenio->dni == Input::get('dniduenio')){
				//El conductor continua con el mismo auto y el mismo duenio
				//Guardamos sus datos
				actualizarconductor($id);
				actualizarduenio();
				$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
				actualizarauto($id, $duenio->id);
				$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
				return Redirect::to('conductor')->with('verde', $mensaje);
			}else{
				//el conductor continua con el mismo auto pero se cambia al dueño
				//verificamos si el nuevo dueño existe en la tabla personas.
				$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
				if($duenio){
					//el nuevo dueño existe en la tabla personas.
					//actualizamos al conductor, 
					actualizarconductor($id);
					actualizarduenio();
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
					actualizarauto($id, $duenio->id);
					$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
					return Redirect::to('conductor')->with('verde', $mensaje);
				}else{
					//el nuevo dueño no existe en la tabla personas
					//actualizamos conductor, creamos al dueño y actualizamos el auto
					actualizarconductor($id);
					nuevoduenio();
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
					actualizarauto($id, $duenio->id);
					$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
					return Redirect::to('conductor')->with('verde', $mensaje);
				}
			}
		}else{
			//el conductor va a cambiar de auto, vemos si el auto existe en la tabla autos
			$auto = Auto::where('placa', '=', strtoupper(Input::get('placa')))->count();
			if($auto){
				//si el uto existe en la tabla autos hay que ver si se esta cambiando su dueño
				$auto = Auto::where('placa', '=', strtoupper(Input::get('placa')))->first();
				$duenio = Persona::find($auto->duenio);
				if($duenio->dni == Input::get('dniduenio')){
					//es el mismo dueño, actualizamos al conductor al dueño y al auto. borramos licencia del anterior conductor.
					$conductor = Persona::find($auto->conductor)->first();
					$licencia = $conductor->licencia;
					$licencia->delete();
					$auto = Auto::where('conductor', '=', $id)->first();
					$auto->delete();
					actualizarconductor($id);
					actualizarduenio();
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
					actualizarauto($id, $duenio->id);
					$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
					return Redirect::to('conductor')->with('verde', $mensaje);
				}else{
					//esta cambiando de dueño, ahora veremos si el nuevo dueño existe en la tabla personas
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
					if($duenio){
						//el nuevo dueño existe en la tabla personas.
						//actualizamos al conductor, al dueño y el auto
						$conductor = Persona::find($auto->conductor);
						$licencia = $conductor->licencia;
						$licencia->delete();
						$auto = Auto::where('conductor', '=', $id)->first();
						$auto->delete();
						actualizarconductor($id);
						actualizarduenio();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						actualizarauto($id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}else{
						//el nuevo dueño no está en la tabla personas.
						//actualizamos los datos del conductor, creamos el nuevo dueño y actualizamos los datos del auto
						$conductor = Persona::find($auto->conductor);
						$licencia = $conductor->licencia;
						$licencia->delete();
						$auto = Auto::where('conductor', '=', $id)->first();
						$auto->delete();
						actualizarconductor($id);
						nuevoduenio();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						actualizarauto($id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}
				}
			}else{
				//el auto no existe, veremos si el dueño existe.
				$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
				if($duenio){
						//el nuevo dueño existe en la tabla personas.
						//actualizamos al conductor, al dueño y el auto
						$auto = Auto::where('conductor', '=', $id)->first();
						$auto->delete();
						actualizarconductor($id);
						actualizarduenio();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						nuevoauto($id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor');
					}else{
						//el nuevo dueño no está en la tabla personas.
						//actualizamos los datos del conductor, creamos el nuevo dueño y actualizamos los datos del auto
						$auto = Auto::where('conductor', '=', $id)->first();
						$auto->delete();
						actualizarconductor($id);
						nuevoduenio();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						nuevoauto($id, $duenio->id);
						$mensaje = "LOS DATOS DEL CONDUCTOR Y EL AUTO QUE CONDUCE FUERON ACTUALIZADOS SATISFACTORIAMENTE.";
						return Redirect::to('conductor')->with('verde', $mensaje);
					}
			}
		}
	}

	public function destroy($id)
	{
		$conductor = Persona::find($id);
		$licencia = $conductor->licencia;
		$licencia->delete();
		$auto = Auto::where('conductor', '=', $id)->first();
		$auto->delete();
		$mensaje = "LOS DATOS DEL CONDUCTOR ".$conductor->nombre." ".$conductor->apellidos.
		" Y EL AUTO QUE CONDUCE FUERON BORRADOS DE LA EMPRESA, NO PODRA ENCONTRARLO EN LA LISTA.";
		return Redirect::to('conductor')->with('naranja', $mensaje);
	}


}