<?php

class ErrorController extends BaseController{

	public function getConductor(){

		return View::make('error.conductor');
	}

	public function getAuto(){

		return View::make('error.auto');
	}

	public function getDuenio(){

		return View::make('error.duenio');
	}

	public function getUsuario(){
		return $usuario;
		//return View::make('error.usuario');
	}
}