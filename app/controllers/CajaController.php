<?php

class CajaController extends \BaseController {

	public function index()
	{
		//
	}

	public function create()
	{
		
	}

	public function store()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$usuarios = DB::table('usuarios')->get();

		foreach($usuarios as $usuario){

			$cajeros[] = $usuario->id;
		}

		for($i=0; $i<count($cajeros); $i++){

			if(Input::get('usuario'.$cajeros[$i]) == $cajeros[$i]){

				$cajero[] = Input::get('usuario'.$cajeros[$i]);
			}
		}

		for($i=0; $i<count($cajero); $i++){

			$usuario = Usuario::find($cajero[$i]);
			$usuario->contrasenia = password_hash(Input::get('contrasenia'), PASSWORD_DEFAULT);
			$usuario->save();
		}

		$mensaje = "ACABA DE AUTORIZAR LA APERTURA DE CAJA A DIFERENTES USUARIOS. INFORMELES SU NUEVA CONTRASEÑA PARA 
		INICIAR SUS MOVIMIENTOS O VENTAS.";
		return Redirect::to('buscar/usuarios')->with('verde', $mensaje);
	}

	public function show($id)
	{
		function password($length,$uc,$n,$sc)
		{
		    $source = 'abcdefghijklmnopqrstuvwxyz';
		    if($uc==1) $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    if($n==1) $source .= '1234567890';
		    if($sc==1) $source .= '|@#~$%()=^*+[]{}-_';
		    if($length>0){
		        $rstr = "";
		        for($i=1; $i<=$length; $i++){
		        	$rstr .= substr($source,rand(0,strlen($source)),1);
		        }
		 
		    }
		    $hash = password_hash($rstr, PASSWORD_DEFAULT);
		    return $hash;
		}

		$cajero = Usuario::find($id);
		$cierre = Cierre::find($cajero->cierre);

		$cajero->contrasenia = password(11, TRUE, TRUE, TRUE);
		$cajero->caja = 0;
		$cajero->cierre = 0;
		$cajero->save();

		$cierre->estado = 0;
		$cierre->save();

		$mensaje = "ACABA DE CERRAR CAJA AL USUARIO ".Persona::find($cajero->personas_id)->nombre." ".
		Persona::find($cajero->personas_id)->apellidos.", RECOMENDAMOS COMUNICARSE CON EL USUARIO AFECTADO
		AVISANDO SU DECISIÓN Y COORDINANDO EL ESTADO DE LA CAJA. RECOJER S/. ".($cierre->total + $cierre->inicio).".00";
	  	return Redirect::to('administrador')->with('naranja', $mensaje);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
