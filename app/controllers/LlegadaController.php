<?php

class LlegadaController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$dia = getdate();
		$hoy = $dia[0]-$dia["seconds"]-$dia["hours"]*3600-$dia["minutes"]*60 - 86400;
		$maniana = $hoy + 86400;
		$contratos = DB::table('contratos')->where('servicios_id', '=', 1)
		->where('agencia', '=', Auth::user()->agencias_id)->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {
				
				return View::make('llegada.inicio')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				return View::make('llegada.inicio')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}
	}

	public function create()
	{
		//
	}

	public function store()
	{
		function numtoletras($xcifra)
		{
		    $xarray = array(0 => "Cero",
		        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		    );

		    $xcifra = trim($xcifra);
		    $xlength = strlen($xcifra);
		    $xpos_punto = strpos($xcifra, ".");
		    $xaux_int = $xcifra;
		    $xdecimales = "00";
		    if (!($xpos_punto === false)) {
		        if ($xpos_punto == 0) {
		            $xcifra = "0" . $xcifra;
		            $xpos_punto = strpos($xcifra, ".");
		        }
		        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		    }

		    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT);
		    $xcadena = "";
		    for ($xz = 0; $xz < 3; $xz++) {
		        $xaux = substr($XAUX, $xz * 6, 6);
		        $xi = 0;
		        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		        $xexit = true; // bandera para controlar el ciclo del While
		        while ($xexit) {
		            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
		                break; // termina el ciclo
		            }

		            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
		            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
		            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
		                switch ($xy) {
		                    case 1: // checa las centenas
		                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
		                            
		                        } else {
		                            $key = (int) substr($xaux, 0, 3);
		                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
		                                $xseek = $xarray[$key];
		                                $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
		                                if (substr($xaux, 0, 3) == 100)
		                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
		                            }
		                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
		                                $key = (int) substr($xaux, 0, 1) * 100;
		                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
		                                $xcadena = " " . $xcadena . " " . $xseek;
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 0, 3) < 100)
		                        break;
		                    case 2: // checa las decenas (con la misma lógica que las centenas)
		                        if (substr($xaux, 1, 2) < 10) {
		                            
		                        } else {
		                            $key = (int) substr($xaux, 1, 2);
		                            if (TRUE === array_key_exists($key, $xarray)) {
		                                $xseek = $xarray[$key];
		                                $xsub = subfijo($xaux);
		                                if (substr($xaux, 1, 2) == 20)
		                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3;
		                            }
		                            else {
		                                $key = (int) substr($xaux, 1, 1) * 10;
		                                $xseek = $xarray[$key];
		                                if (20 == substr($xaux, 1, 1) * 10)
		                                    $xcadena = " " . $xcadena . " " . $xseek;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 1, 2) < 10)
		                        break;
		                    case 3: // checa las unidades
		                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
		                            
		                        } else {
		                            $key = (int) substr($xaux, 2, 1);
		                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
		                            $xsub = subfijo($xaux);
		                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                        } // ENDIF (substr($xaux, 2, 1) < 1)
		                        break;
		                } // END SWITCH
		            } // END FOR
		            $xi = $xi + 3;
		        } // ENDDO

		        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
		            $xcadena.= " DE";

		        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
		            $xcadena.= " DE";

		        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		        if (trim($xaux) != "") {
		            switch ($xz) {
		                case 0:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN BILLON ";
		                    else
		                        $xcadena.= " BILLONES ";
		                    break;
		                case 1:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN MILLON ";
		                    else
		                        $xcadena.= " MILLONES ";
		                    break;
		                case 2:
		                    if ($xcifra < 1) {
		                        $xcadena = "CERO NUEVOS SOLES CON $xdecimales/100 CENTIMOS";
		                    }
		                    if ($xcifra >= 1 && $xcifra < 2) {
		                        $xcadena = "UN NUEVO SOL CON $xdecimales/100 CENTIMOS";
		                    }
		                    if ($xcifra >= 2) {
		                        $xcadena.= " NUEVOS SOLES CON $xdecimales/100 CENTIMOS"; //
		                    }
		                    break;
		            } // endswitch ($xz)
		        } // ENDIF (trim($xaux) != "")
		        // ------------------      en este caso, para México se usa esta leyenda     ----------------
		        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		    }
		    return trim($xcadena);
		}

		function subfijo($xx)
		{
		    $xx = trim($xx);
		    $xstrlen = strlen($xx);
		    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		        $xsub = "";

		    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		        $xsub = "MIL";

		    return $xsub;
		}

		$contrato = new Contrato;
		$contrato->servicios_id = 1;
		$contrato->costo = substr(Input::get('costo'), 4);
		$contrato->cajero = Persona::find(Auth::user()->personas_id)->nombre
			." ".Persona::find(Auth::user()->personas_id)->apellidos;
		$contrato->cliente = Input::get('persona');
		$contrato->conductor = Persona::find(Input::get('persona'))->nombre
			." ".Persona::find(Input::get('persona'))->apellidos;
		$contrato->auto = Auto::find(Input::get('auto'))->placa;
		$contrato->salida = Agencia::find(Auth::user()->agencias_id)->nombre;
		$contrato->destino = Input::get('destino');
		$contrato->estado = 1;
		$contrato->agencia = Auth::user()->agencias_id;
		$contrato->save();

		$letras = numtoletras($contrato->costo);

		$cierre = Cierre::find(Auth::user()->cierre);
		$cierre->total += substr(Input::get('costo'), 4);
		$cierre->save();

		return View::make('llegada.recibo')->with('llegada', $contrato)->with('letras', $letras);
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {
				
				
				$llegada = Contrato::find($id);
				return View::make('llegada.mostrar')->with('llegada', $llegada)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				$llegada = Contrato::find($id);
				return View::make('llegada.mostrar')->with('llegada', $llegada)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador');
			}else{

				return Redirect::to('trabajador');
			}
		}

	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		
		$llegada = Contrato::find($id);
		
		$hoy = (time()-18000)-((time()-18000)%86400);
		$maniana = $hoy + 86400;
		$pasajes = DB::table('contratos')->where('servicios_id', '=', 2)
		->where('estado', '=', 1)->where('agencia', '=', Auth::user()->agencias_id)->orWhere('servicios_id', '=', 3)
		->where('agencia', '=', Auth::user()->agencias_id)->where('estado', '=', 1)->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {
				
				return View::make('pasaje.viajar')->with('pasajes', $pasajes)->with('hoy', $hoy)
				->with('maniana', $maniana)->with('llegada', $llegada)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				
				return View::make('pasaje.viajar')->with('pasajes', $pasajes)->with('hoy', $hoy)
				->with('maniana', $maniana)->with('llegada', $llegada)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador');
			}else{

				return Redirect::to('administrador');
			}
		}

	}

	public function update($id)
	{

		$pasajes = DB::table('contratos')->where('servicios_id', '=', 2)
		->where('estado', '=', 1)->orWhere('servicios_id', '=', 3)
		->where('estado', '=', 1)->orderBy('id')->get();
		foreach($pasajes as $pasaje){
			$pasajeros[] = $pasaje->id;
		}

		$pasajero = [];

		for($i=0; $i<count($pasajeros); $i++){
			if(Input::get('pasajero'.$pasajeros[$i]) == $pasajeros[$i]){
				$pasajero[] = Input::get('pasajero'.$pasajeros[$i]);
			}
		}

		if (count($pasajero) == 0) {

			$mensaje = "NO SELECCIONO NINGUN PASAJERO O ENCOMIENDA PARA LLEVAR";
			return Redirect::TO('llegada/'.$id.'/edit')->with('rojo', $mensaje);
		}

		for($i=0; $i<count($pasajero); $i++){
			$pasaje = Contrato::find($pasajero[$i]);
			$llegada = Contrato::find($id);
			$pasaje->conductor = $llegada->conductor;
			$pasaje->auto = $llegada->auto;
			$pasaje->estado = 0;
			$pasaje->save();
		}

		$llegada = Contrato::find($id);
		$llegada->destino = strtoupper(Input::get('destino'));
		$llegada->estado = 0;
		$llegada->save();

		$mensaje = "LOS PASAJEROS Y ENCOMIENDAS FUERON SUBIDOS CORRECTAMENTE AL AUTO ".$llegada->auto.
		" Y ESTAN EN CAMINO A SU DESTINO HASTA LLEGAR A ".$llegada->destino.". SUGERIMOS VALLA SUPERVISANDO 
		EL RECOJO DE LAS ENCOMIENDAS ENVIADAS.";

		return View::make('llegada.relacion')->with('pasajero', $pasajero)->with('llegada', $llegada);
	}

	public function destroy($id)
	{
		$llegada = Contrato::find($id);
		$llegada->estado = 0;
		$llegada->save();
		return Redirect::to('llegada');
	}

	
}
