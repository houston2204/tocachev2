<?php

class BuscarController extends BaseController{

	public function getConductor(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$licencia = new Licencia;
		$persona = new Persona;
		$auto = new Auto;
		$autos = DB::table('autos')->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {

				return View::make('llegada.buscar')->with('licencia', $licencia)->with('autos', $autos)
				->with('persona', $persona)->with('auto', $auto)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				return View::make('llegada.buscar')->with('licencia', $licencia)->with('autos', $autos)
				->with('persona', $persona)->with('auto', $auto)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);

			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}	
	}

	public function postBuscarConductor(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$auto = Auto::where('placa', '=', strtoupper(Input::get('placa')))->first();
		if ($auto != null) {
			
			$persona = Persona::find($auto->conductor);
			$licencia = Licencia::find($persona->id);
			
			return View::make('llegada.guardar')->with('licencia', $licencia)
			->with('persona', $persona)->with('auto', $auto)->with('notificaciones', $notificaciones)
			->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
		}else{

			$mensaje = "EL AUTO BUSCADO NO SE ENCUENTRA REGISTRADO EN LA EMPRESA. LE SUGERIMOS COMUNICARSE CON 
			EL ADMINISTRADOR A CARGO.";
			return Redirect::to('buscar/conductor')->with('rojo', $mensaje);
		}
	}

	public function getCliente(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$persona = new Persona;

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {

				return View::make('pasaje.buscar')->with('persona', $persona)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				
				return View::make('pasaje.buscar')->with('persona', $persona)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}	
	}

	public function postBuscarCliente(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$hoydia = date('d-m-Y H:i:s', strtotime('-5 hours'));
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;


		$persona = Persona::where('dni', '=', Input::get('dni'))->first();
		$pasaje = Contrato::find(Input::get('pasaje'));

		if ($persona != null) {
			if($pasaje != null){

				return View::make('pasaje.guardar')->with('persona', $persona)->with('pasaje', $pasaje)
				->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				$pasaje = new Contrato;
				return View::make('pasaje.guardar')->with('persona', $persona)->with('pasaje', $pasaje)
				->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{
			if($pasaje != null){
				$persona = new Persona;
				return View::make('pasaje.nuevo')->with('dni', Input::get('dni'))->with('pasaje', $pasaje)
				->with('persona', $persona)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw)->with('hoydia', $hoydia);
			}else{
				$persona = new Persona;
				$pasaje = new Contrato;
				return View::make('pasaje.nuevo')->with('dni', Input::get('dni'))->with('pasaje', $pasaje)
				->with('persona', $persona)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw)->with('hoydia', $hoydia);
			}
		}
	}

	public function getRemitente(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$encomienda = new Contrato;
		$remitente = new Persona;
		$destinatario = new Persona;

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {

				return View::make('encomienda.buscarremitente')->with('encomienda', $encomienda)
				->with('remitente', $remitente)
				->with('destinatario', $destinatario)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				return View::make('encomienda.buscarremitente')->with('encomienda', $encomienda)
				->with('remitente', $remitente)
				->with('destinatario', $destinatario)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}
	}

	public function postBuscarRemitente(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$encomienda = Contrato::find(Input::get('encomienda'));

		if ($remitente != null) {
			if ($encomienda != null) {

				$destinatario = Persona::find($encomienda->receptor);

				return View::make('encomienda.buscardestinatario')->with('remitente', $remitente)
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				$encomienda = new Contrato;
				$destinatario = new Persona;

				return View::make('encomienda.buscardestinatario')->with('remitente', $remitente)
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{
			if ($encomienda != null) {
				
				$destinatario = Persona::find($encomienda->receptor);

				return View::make('encomienda.nuevobuscardestinatario')->with('dniremitente', Input::get('dniremitente'))
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				$encomienda = new Contrato;
				$destinatario = new Persona;
				$remitente = new Persona;

				return View::make('encomienda.nuevobuscardestinatario')->with('dniremitente', Input::get('dniremitente'))
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)->with('remitente', $remitente)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}
	}

	public function postBuscarDestinatario(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;


		$usuario = Input::get('usuario');
		$ruc = Input::get('ruc');
		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$destinatario = Persona::where('dni', '=', Input::get('dnidestinatario'))->first();
		$encomienda = Contrato::find(Input::get('encomienda'));

		if ($remitente != null) {
			
			if ($destinatario != null) {

				if($encomienda != null){

					return View::make('encomienda.guardar')->with('remitente', $remitente)
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);	
				}else{
					
					$encomienda = new Contrato;

					return View::make('encomienda.guardar')->with('remitente', $remitente)
					->with('destinatario', $destinatario)->with('encomienda', $encomienda)
					->with('usuario', $usuario)->with('ruc', $ruc)->with('notificaciones', $notificaciones)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}else{

				if ($encomienda != null) {

					$destinatario = new Persona;

					return View::make('encomienda.nuevodestinatario')->with('remitente', $remitente)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('destinatario', $destinatario)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$encomienda = new Contrato;
					$destinatario = new Persona;

					return View::make('encomienda.nuevodestinatario')->with('remitente', $remitente)
					->with('encomienda', $encomienda)->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)
					->with('sw', $sw);
				}
			}
		}else{

			if ($destinatario != null) {

				if ($encomienda != null) {

					$remitente = new Persona;
					
					return View::make('encomienda.nuevoremitente')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$encomienda = new Contrato;
					$remitente = new Persona;

					return View::make('encomienda.nuevoremitente')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}else{

				if ($encomienda != null) {
					
					$remitente = new Persona;
					$destinatario = new Persona;

					return View::make('encomienda.nuevo')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$remitente = new Persona;
					$destinatario = new Persona;
					$encomienda = new Contrato;

					return View::make('encomienda.nuevo')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}
		}
	}

	public function getDeposito(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$giro = new Contrato;
		$remitente = new Persona;
		$destinatario = new Persona;

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {

				return View::make('giro.buscarremitente')->with('giro', $giro)->with('remitente', $remitente)
				->with('destinatario', $destinatario)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{
				return View::make('giro.buscarremitente')->with('giro', $giro)->with('remitente', $remitente)
				->with('destinatario', $destinatario)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}		
	}

	public function postBuscarDeposito(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$encomienda = Contrato::find(Input::get('encomienda'));

		if ($remitente != null) {
			if ($encomienda != null) {

				$destinatario = Persona::find($encomienda->receptor);

				return View::make('giro.buscardestinatario')->with('remitente', $remitente)
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				$encomienda = new Contrato;
				$destinatario = new Persona;

				return View::make('giro.buscardestinatario')->with('remitente', $remitente)
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{
			if ($encomienda != null) {
				
				$destinatario = Persona::find($encomienda->receptor);

				return View::make('giro.nuevobuscardestinatario')->with('dniremitente', Input::get('dniremitente'))
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				$encomienda = new Contrato;
				$destinatario = new Persona;
				$remitente = new Persona;

				return View::make('giro.nuevobuscardestinatario')->with('dniremitente', Input::get('dniremitente'))
				->with('encomienda', $encomienda)->with('destinatario', $destinatario)->with('remitente', $remitente)
				->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}
	}

	public function postBuscarCobrador(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;


		$usuario = Input::get('usuario');
		$ruc = Input::get('ruc');
		$remitente = Persona::where('dni', '=', Input::get('dniremitente'))->first();
		$destinatario = Persona::where('dni', '=', Input::get('dnidestinatario'))->first();
		$encomienda = Contrato::find(Input::get('encomienda'));

		if ($remitente != null) {
			
			if ($destinatario != null) {

				if($encomienda != null){

					return View::make('giro.guardar')->with('remitente', $remitente)
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);	
				}else{
					
					$encomienda = new Contrato;

					return View::make('giro.guardar')->with('remitente', $remitente)
					->with('destinatario', $destinatario)->with('encomienda', $encomienda)
					->with('usuario', $usuario)->with('ruc', $ruc)->with('notificaciones', $notificaciones)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}else{

				if ($encomienda != null) {

					$destinatario = new Persona;

					return View::make('giro.nuevodestinatario')->with('remitente', $remitente)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('destinatario', $destinatario)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$encomienda = new Contrato;
					$destinatario = new Persona;

					return View::make('giro.nuevodestinatario')->with('remitente', $remitente)
					->with('encomienda', $encomienda)->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)
					->with('sw', $sw);
				}
			}
		}else{

			if ($destinatario != null) {

				if ($encomienda != null) {

					$remitente = new Persona;
					
					return View::make('giro.nuevoremitente')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$encomienda = new Contrato;
					$remitente = new Persona;

					return View::make('giro.nuevoremitente')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))
					->with('destinatario', $destinatario)->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}else{

				if ($encomienda != null) {
					
					$remitente = new Persona;
					$destinatario = new Persona;

					return View::make('giro.nuevo')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}else{

					$remitente = new Persona;
					$destinatario = new Persona;
					$encomienda = new Contrato;

					return View::make('giro.nuevo')->with('dniremitente', Input::get('dniremitente'))
					->with('nombreremitente', Input::get('nombreremitente'))->with('remitente', $remitente)
					->with('apellidosremitente', Input::get('apellidosremitente'))->with('destinatario', $destinatario)
					->with('dnidestinatario', Input::get('dnidestinatario'))->with('usuario', $usuario)->with('ruc', $ruc)
					->with('notificaciones', $notificaciones)->with('encomienda', $encomienda)
					->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
				}
			}
		}
	}

	public function getUsuarios(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$usuarios = DB::table('usuarios')->get();

		return View::make('cierre.usuarios')->with('usuarios', $usuarios)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function getApertura(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		if (Auth::user()->caja) {

			$mensaje = "YA ABRIO CAJA ANTERIORMENTE Y NO LO HA CERRADO, POR FAVOR REVISE SU ESTADO DE CAJA EN CIERRE.";
			
			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('naranja', $mensaje);
			}else{

				return Redirect::to('administrador')->with('naranja', $mensaje);
			}
		}else{
			
			return View::make('cierre.aperturar')->with('notificaciones', $notificaciones)
			->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
		}
	}

	public function getCerrar(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		if (Auth::user()->caja) {
			
			return View::make('cierre.cerrar')->with('notificaciones', $notificaciones)
			->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
		}else{

			$mensaje = "AÚN NO ABRE CAJA PARA QUE PUEDA CERRAR UNA, COMUNÍQUESE CON EL ADMINISTRADOR A CARGO PARA 
			OBTENER SU NUEVA CONTRASEÑA Y PUEDA ABRIR SU CAJA.";
			
			return Redirect::to('administrador')->with('naranja', $mensaje);
		}
	}

	public function getTerminal(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$dia = getdate();
		$hoy = $dia[0]-$dia["seconds"]-$dia["hours"]*3600-$dia["minutes"]*60 - 86400;
		$maniana = $hoy + 86400;
		$contratos = DB::table('contratos')->where('servicios_id', '=', 1)->where('estado', '=', 1)
		->where('agencia', '=', Auth::user()->agencias_id)->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {
				
				return View::make('pasaje.terminal')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				return View::make('pasaje.terminal')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}
	}

	public function postPasajes(){

		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$contrato = Contrato::find(Input::get('contrato'));
		return $contrato;

		$dia = getdate();
		$hoy = $dia[0]-$dia["seconds"]-$dia["hours"]*3600-$dia["minutes"]*60 - 86400;
		$maniana = $hoy + 86400;
		$contratos = DB::table('contratos')->where('servicios_id', '=', 1)->where('estado', '=', 1)
		->where('agencia', '=', Auth::user()->agencias_id)->get();

		if(Auth::user()->caja){

			if (Auth::user()->tipo) {
				
				return View::make('pasaje.auto')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}else{

				return View::make('pasaje.auto')->with('contratos', $contratos)
				->with('hoy', $hoy)->with('maniana', $maniana)->with('notificaciones', $notificaciones)
				->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
			}
		}else{

			$mensaje = "USTED AÚN NO APERTURA CAJA PARA HACER USO DE ESTA OPCIÓN. LE RECOMENDAMOS COMUNICARSE CON EL 
			ADMINISTRADOR A CARGO.";

			if (Auth::user()->tipo) {
				
				return Redirect::to('administrador')->with('rojo', $mensaje);
			}else{

				return Redirect::to('administrador')->with('rojo', $mensaje);
			}
		}
	}

	public function postLlega(){
		
		$encomienda = Contrato::find(Input::get('encomienda'));
		$encomienda->estado = 2;
		$encomienda->save();

		return Redirect::to('encomienda');
	}
}