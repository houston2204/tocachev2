<?php

class AutoController extends \BaseController {

	public function index()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;
		
		$autos = DB::table('autos')->get();
		return View::make('auto.inicio')->with('autos', $autos)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function create()
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$direccioncond = new Direccion;
		$conductor = new Persona;
		$licencia = new Licencia;
		$direccionduen = new Direccion;
		$duenio = new Persona;
		$auto = new Auto;
		$seguro = new Seguro;

		return View::make('auto.nuevo')->with('conductor', $conductor)
		->with('direccioncond', $direccioncond)->with('licencia', $licencia)
		->with('auto', $auto)->with('seguro', $seguro)
		->with('duenio', $duenio)->with('direccionduen', $direccionduen)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function store()
	{
		function formatearfecha($fecha){
			$formato = $fecha;
			$dia = substr($formato, 0, 2);
			$mes = substr($formato, 3, 2);
			$anio = substr($formato, 6, 4);
			$formato = $anio."-".$mes."-".$dia;
			return $formato;
		}

		function guardarconductor(){
			$direccion = new Direccion;
			$persona = new Persona;
			$licencia = new Licencia;

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->dni = strtoupper(Input::get('dniconductor'));
			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = strtoupper(Input::get('telefonoconductor'));
			$persona->direcciones_id = $direccion->id;
			$persona->save();

			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $persona->id;
			$licencia->save();
		}

		function guardarduenio(){
			$direccion = new Direccion;
			$persona = new Persona;

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->dni = Input::get('dniduenio');
			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->direcciones_id = $direccion->id;
			$persona->save();
		}

		function guardarauto($conductor, $duenio){
			$auto = new Auto;
			$seguro = new Seguro;

			$auto->placa = strtoupper(Input::get('placa'));
			$auto->marca = strtoupper(Input::get('marca'));
			$auto->modelo = strtoupper(Input::get('modelo'));
			$auto->color = strtoupper(Input::get('color'));
			$auto->conductor = $conductor;
			$auto->duenio = $duenio;
			$auto->save();

			$seguro->numero = strtoupper(Input::get('seguro'));
			$seguro->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('vencseguro'))));
			$seguro->autos_id = $auto->id;
			$seguro->save();
		}

		function guardarlicencia($conductor){
			$licencia = new Licencia;

			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $conductor;
			$licencia->save();
		}

		//verificamos si el conductro existe.
		$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->count();
		if($conductor){
			//Si existe el conductor verificamos si conduce un auto
			$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
			$auto = Auto::where('conductor', '=', $conductor->id)->count();
			if ($auto) {
				//si conduce redireccionamos a una página de error
				//El conductor ya maneja un auto
				$mensaje = "PARECE QUE EL CONDUCTOR QUE QUIERE ASIGNARLE A ESTE AUTO YA CONDUCE UN AUTO, LE DEJAMOS 
				POR ACA PARA QUE ACTUALICE LOS DATOS DEL CONDUCTOR Y SU AUTO Y FUESE EL CASO.";
				return Redirect::to('conductor/'.$conductor->id.'/edit')->with('naranja', $mensaje);
			}else{
				//Si no conduce un auto verificamos si el auto existe
				$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->count();
				if($auto){
					//Si el auto existe redireccionamos a una pagina de error.
					//el auto ya es manejado por otro.
					$auto = Auto::where('conductor', '=', $conductor->id)->first();
					$mensaje = "PARECE QUE EL AUTO QUE QUIERE INGRESAR A LA EMPRESA YA ESTA EN LA EMPRESA, EN ESTE FORMULARIO 
					PODRA MODIFICAR LOS DATOS DEL AUTO Y SU CONDUCTOR SI FUESE EL CASO.";
					return Redirect::to('auto/'.$auto->id.'/edit')->with('naranja', $mensaje);
				}else{
					//si el auto no existe verificamos si el dueño existe
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
					if($duenio){
						//si el dueño existe guardamos los datos del conductor y el auto con los datos
						//del dueño ya existente y redireccionamos a la lista de conductores.
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						guardarlicencia($conductor->id);
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON INGRESADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}else{
						//si el dueño no existe guardamos la licencia del conductor, el duenño y el auto 
						//y redireccionamos a la lista de conductores.
						guardarduenio();
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						guardarlicencia($conductor->id);
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON INGRESADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}
				}
			}
		}else{
			//Si el conductor no existe verificamos si el auto existe
			$auto = Auto::where('placa', '=', strtoupper(input::get('placa')))->count();
			if($auto){
				//Si el auto existe redireccionamos a una pagina de error.
				//el auto ya es manejado por otro.
				$auto = Auto::where('conductor', '=', $conductor->id)->first();
				$mensaje = "PARECE QUE EL AUTO QUE QUIERE INGRESAR A LA EMPRESA YA ESTA EN LA EMPRESA, EN ESTE FORMULARIO 
				PODRA MODIFICAR LOS DATOS DEL AUTO Y SU CONDUCTOR SI FUESE EL CASO.";
				return Redirect::to('auto/'.$auto->id.'/edit')->with('naranja', $mensaje);
			}else{
				//si el auto no existe verificamos si el dueño existe
				$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
				if($duenio){
					//si el dueño existe guardamos los datos del conductor y el auto con los datos
					//del dueño ya existente y redireccionamos a la lista de conductores.
					guardarconductor();
					$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
					guardarauto($conductor->id, $duenio->id);
					$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON INGRESADOS CON EXITO.";
					return Redirect::to('auto')->with('verde', $mensaje);
				}else{
					//si el dueño no existe verificamos si el conductor y el dueño son la misma persona.
					if(input::get('dniconductor') == input::get('dniduenio')){
						//si son la misma persona guardamo al conductor y el auto con los datos del conductor.
						guardarconductor();
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						guardarauto($conductor->id, $conductor->id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON INGRESADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}else{
						//si no son la misma persona guardamos al conductor, dueño y el auto.
						guardarconductor();
						guardarduenio();
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						guardarauto($conductor->id, $duenio->id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON INGRESADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}
				}
			}
		}
	}

	public function show($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$auto = Auto::find($id);
		return View::make('auto.mostrar')->with('auto', $auto)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function edit($id)
	{
		$lv = 0;
		$lw = 0;
		$sv = 0;
		$sw = 0;
		$hoy = date('Y-m-d');
		$mes = date('Y-m-d', strtotime('+1 month'));
		$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
		$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
		$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
		foreach ($licenciasvenc as $licencia) {
			$lv++;
		}

		foreach ($licenciasxvencer as $licencia) {
			$lw++;
		}

		foreach ($soatvenc as $soat) {
			$sv++;
		}

		foreach ($soatxvencer as $soat) {
			$sw++;
		}

		$notificaciones = $lv + $lw + $sv + $sw;

		$auto = Auto::find($id);
		$seguro = $auto->seguro;
		$duenio = Persona::find($auto->duenio);
		$direccionduen = Direccion::find($duenio->direcciones_id);
		$conductor = Persona::find($auto->conductor);
		$direccioncond = Direccion::find($conductor->direcciones_id);
		$licencia = $conductor->licencia;

		return View::make('auto.nuevo')->with('conductor', $conductor)
		->with('direccioncond', $direccioncond)->with('licencia', $licencia)
		->with('auto', $auto)->with('seguro', $seguro)
		->with('duenio', $duenio)->with('direccionduen', $direccionduen)->with('notificaciones', $notificaciones)
		->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
	}

	public function update($id)
	{
		function formatearfecha($fecha){
			$formato = $fecha;
			$dia = substr($formato, 0, 2);
			$mes = substr($formato, 3, 2);
			$anio = substr($formato, 6, 4);
			$formato = $anio."-".$mes."-".$dia;
			return $formato;
		}

		function guardarconductor(){
			$direccion = new Direccion;
			$persona = new Persona;
			$licencia = new Licencia;

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->dni = Input::get('dniconductor');
			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = Input::get('telefonoconductor');
			$persona->direcciones_id = $direccion->id;
			$persona->save();

			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $persona->id;
			$licencia->save();
		}

		function actualizarconductor(){
			$persona = Persona::where('dni', '=', Input::get('dniconductor'))->first();
			$direccion = Direccion::find($persona->direcciones_id);
			$licencia = $persona->licencia;

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = Input::get('telefonoconductor');
			$persona->save();

			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->save();
		}

		function actualizarnuevoconductor(){
			$persona = Persona::where('dni', '=', Input::get('dniconductor'))->first();
			$direccion = Direccion::find($persona->direcciones_id);

			$direccion->lugar = strtoupper(Input::get('lugarconductor'));
			$direccion->distrito = strtoupper(Input::get('distritoconductor'));
			$direccion->provincia = strtoupper(Input::get('provinciaconductor'));
			$direccion->departamento = strtoupper(Input::get('departamentoconductor'));
			$direccion->save();

			$persona->nombre = strtoupper(Input::get('nombreconductor'));
			$persona->apellidos = strtoupper(Input::get('apellidosconductor'));
			$persona->telefono = Input::get('telefonoconductor');
			$persona->save();
		}

		function guardarduenio(){
			$direccion = new Direccion;
			$persona = new Persona;

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->dni = Input::get('dniduenio');
			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->direcciones_id = $direccion->id;
			$persona->save();
		}

		function actualizarduenio(){
			$persona = Persona::where('dni', '=', Input::get('dniduenio'))->first();
			$direccion = Direccion::find($persona->direcciones_id);

			$direccion->lugar = strtoupper(Input::get('lugarduenio'));
			$direccion->distrito = strtoupper(Input::get('distritoduenio'));
			$direccion->provincia = strtoupper(Input::get('provinciaduenio'));
			$direccion->departamento = strtoupper(Input::get('departamentoduenio'));
			$direccion->save();

			$persona->nombre = strtoupper(Input::get('nombreduenio'));
			$persona->apellidos = strtoupper(Input::get('apellidosduenio'));
			$persona->telefono = Input::get('telefonoduenio');
			$persona->save();
		}

		function actualizarauto($conductor, $duenio, $id){
			$auto = Auto::find($id);
			$seguro = Seguro::where('autos_id', '=', $auto->id)->first();

			$auto->marca = strtoupper(Input::get('marca'));
			$auto->modelo = strtoupper(Input::get('modelo'));
			$auto->color = strtoupper(Input::get('color'));
			$auto->conductor = $conductor;
			$auto->duenio = $duenio;
			$auto->save();

			$seguro->numero = strtoupper(Input::get('seguro'));
			$seguro->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('vencseguro'))));
			$seguro->save();
		}

		function guardarlicencia($conductor){
			$licencia = new Licencia;

			$licencia->codigo = strtoupper(Input::get('codigo'));
			$licencia->numero = strtoupper(Input::get('numlicencia'));
			$licencia->categoria = strtoupper(Input::get('categoria'));
			$licencia->vencimiento = date('Y-m-d', strtotime(formatearfecha(Input::get('venclicencia'))));
			$licencia->personas_id = $conductor;
			$licencia->save();
		}

		//verificamos si se cambio de dueño
		$auto = Auto::find($id);
		$duenio = Persona::find($auto->duenio);
		if($duenio->dni == Input::get('dniduenio')){
			actualizarduenio();
			//es el mismo dueño, verificamos si cambio de conductor
			$conductor = Persona::find($auto->conductor);
			if($conductor->dni == Input::get('dniconductor')){
				//es el mismo dueño y el mismo conductor
				//Actualizamos los datos del auto, el duenio y el conductor
				actualizarconductor();
				actualizarauto($conductor->id, $duenio->id, $id);
				$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
				return Redirect::to('auto')->with('verde', $mensaje);
			}else{
				//es el mismo dueño pero se cambia de conductor
				//Eliminamos la licencia del anterior conductor.
				$licencia = Licencia::where('personas_id', '=', $conductor->id)->first();
				$licencia->delete();
				//verificamos si el nuevo conductor está en la tabla personas
				$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->count();
				if($conductor){
					//el nuevo conductor esta en la tabla personas
					$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
					//verificamos si conduce un auto.
					$auto = Auto::where('conductor', '=', $conductor->id)->count();
					if($auto){
						//El nuevo conductor del auto maneja una auto
						//Actualizamos los datos del auto, el dueño y del nuevo conductor, 
						//borramos los datos del antiguo auto del nuevo conductor
						actualizarconductor();
						$auto = Auto::where('conductor', '=', $conductor->id)->first();
						$auto->delete();
						actualizarauto($conductor->id, $duenio->id, $id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}else{
						//El nuevo conductor no maneja un auto,
						//Actualizamos los datos del dueño, actualizamos los datos del nuevo conductor,
						//agregamos la licencia del nuevo conductor y actualizamos los datos del auto.
						actualizarnuevoconductor();
						guardarlicencia($conductor->id);
						actualizarauto($conductor->id, $duenio->id, $id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}
				}else{
					//El conductor no esta en la tabla personas.
					//Actualizamos los datos del dueño, creamos al nuevo conductor y su licencia,
					//actualizamos los datos del auto.
					guardarconductor();
					$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
					actualizarauto($conductor->id, $duenio->id, $id);
					$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
					return Redirect::to('auto')->with('verde', $mensaje);
				}
			}
		}else{
			//se esta cambiando de dueño. Verificamos si el nuevo dueño esta en la tabla personas
			$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->count();
			if($duenio){
				$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
				//el dueño está en la tabla personas. Verificamos si se cambió de conductor
				$conductor = Persona::find($auto->conductor);
				if($conductor->dni == Input::get('dniconductor')){
					//es otro dueño y el mismo conductor
					//Actualizamos los datos del dueño, del conductor y del auto
					actualizarduenio();
					actualizarconductor();
					actualizarauto($conductor->id, $duenio->id, $id);
					$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
					return Redirect::to('auto')->with('verde', $mensaje);
				}else{
					//es otro dueño y se cambia de conductor
					$licencia = Licencia::where('personas_id', '=', $conductor->id)->first();
					$licencia->delete();
					//verificamos si el nuevo conductor está en la tabla personas
					$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->count();
					if($conductor){
						//el nuevo conductor esta en la tabla personas
						//verificamos si conduce un auto.
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$auto = Auto::where('conductor', '=', $conductor->id)->count();
						if($auto){
							//El nuevo conductor del auto maneja un auto
							//Actualizamos los datos del auto, el dueño y del nuevo conductor, 
							//borramos los datos del antiguo auto del nuevo conductor
							actualizarduenio();
							actualizarconductor();
							$auto = Auto::where('conductor', '=', $conductor->id)->first();
							$auto->delete();
							actualizarauto($conductor->id, $duenio->id, $id);
							$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
							return Redirect::to('auto')->with('verde', $mensaje);
						}else{
							//El nuevo conductor no maneja un auto,
							//Actualizamos los datos del dueño, actualizamos los datos del nuevo conductor,
							//agregamos la licencia del nuevo conductor y actualizamos los datos del auto.
							actualizarduenio();
							actualizarnuevoconductor();
							guardarlicencia($conductor->id);
							actualizarauto($conductor->id, $duenio->id, $id);
							$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
							return Redirect::to('auto')->with('verde', $mensaje);
						}
					}else{
						//El conductor no esta en la tabla personas.
						//Actualizamos los datos del dueño, creamos al nuevo conductor y su licencia,
						//actualizamos los datos del auto.
						actualizarduenio();
						guardarconductor();
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						actualizarauto($conductor->id, $duenio->id, $id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}
				}
			}else{
				//El nuevo dueño no esta en la tabla personas. Verificamos si se cambió de conductor.
				$conductor = Persona::find($auto->conductor);
				if($conductor->dni == Input::get('dniconductor')){
					//es otro dueño y el mismo conductor
					//Creamos al nuevo dueño, actualizamos los datos del conductor y del auto
					guardarduenio();
					actualizarconductor();
					$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
					actualizarauto($conductor->id, $duenio->id, $id);
					$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
					return Redirect::to('auto')->with('verde', $mensaje);
				}else{
					//es otro dueño pero se cambia de conductor
					$licencia = Licencia::where('personas_id', '=', $conductor->id)->first();
					$licencia->delete();
					//verificamos si el nuevo conductor está en la tabla personas
					$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->count();
					if($conductor){
						//el nuevo conductor esta en la tabla personas
						//verificamos si conduce un auto.
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$auto = Auto::where('conductor', '=', $conductor->id)->count();
						if($auto){
							//El nuevo conductor del auto maneja una auto
							//creamos al nuevo dueño, actualizamos los datos del nuevo conductor, 
							//actualizamos los datos del auto y borramos los datos del antiguo auto del nuevo conductor
							guardarduenio();
							actualizarconductor();
							$auto = Auto::where('conductor', '=', $conductor->id)->first();
							$auto->delete();
							$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
							actualizarauto($conductor->id, $duenio->id, $id);
							$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
							return Redirect::to('auto')->with('verde', $mensaje);
						}else{
							//El nuevo conductor no maneja un auto,
							//Creamos al nuevo dueño, actualizamos los datos del nuevo conductor,
							//agregamos la licencia del nuevo conductor y actualizamos los datos del auto.
							guardarduenio();
							$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
							actualizarnuevoconductor();
							guardarlicencia($conductor->id);
							actualizarauto($conductor->id, $duenio->id, $id);
							$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
							return Redirect::to('auto')->with('verde', $mensaje);
						}
					}else{
						//El conductor no esta en la tabla personas.
						//Creamos al nuevo dueño, creamos al nuevo conductor y su licencia,
						//actualizamos los datos del auto.
						guardarconductor();
						guardarduenio();
						$conductor = Persona::where('dni', '=', Input::get('dniconductor'))->first();
						$duenio = Persona::where('dni', '=', Input::get('dniduenio'))->first();
						actualizarauto($conductor->id, $duenio->id, $id);
						$mensaje = "LOS DATOS DEL AUTO Y SU CONDUCTOR FUERON ACTUALIZADOS CON EXITO.";
						return Redirect::to('auto')->with('verde', $mensaje);
					}
				}
			}
		}
	}

	public function destroy($id)
	{
		//
	}


}
