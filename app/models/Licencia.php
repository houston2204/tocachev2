<?php

Class Licencia extends Eloquent{

	protected $table = 'licencias';

	public $timestamps = false;

	//establecemos la relacion uno a uno
	//con el modelo Persona.
	//Este modelo es el hijo
	public function persona(){
		return $this->belongsTo('Persona', 'personas_id');
	}
}