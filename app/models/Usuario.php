<?php

Class Usuario extends Eloquent{

	protected $table = 'usuarios';

	public $timestamps = false;

	public function persona(){

		return $this->belongsTo('Persona', 'personas_id');
	}
}