<?php

Class Direccion extends Eloquent{

	protected $table = 'direcciones';

	public $timestamps = false;

	//Establecemos la relacion uno a uno con 
	//el modelo hijo Persona
	public function persona(){

		return $this->hasOne('Persona');
	}
}