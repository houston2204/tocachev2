<?php

Class Auto extends Eloquent{

	protected $table = 'autos';

	public $timestamps = false;

	//establecemos la relacion inversa uno a uno con el modelo
	//padre Personas
	public function duenio(){

		return $this->belongsTo('Persona', 'duenio');
	}

	//establecemos la relacion inversa uno a uno con el modelo
	//padre Personas
	public function chofer(){

		return $this->belongsTo('Persona', 'conductor');
	}

	//Establecemos la relacion de uno a uno con el modelo
	//hijo Seguro
	public function seguro(){

		return $this->hasOne('Seguro', 'autos_id');
	}
}