<?php

Class Seguro extends Eloquent{

	protected $table = 'seguros';

	public $timestamps = false;

	//establecemos la relacion inversa uno a uno con 
	//el modelo padre Auto
	public function auto(){

		return $this->belongsTo('Auto', 'autos_id');
	}
}