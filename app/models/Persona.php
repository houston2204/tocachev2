<?php

Class Persona extends Eloquent{

	protected $table = 'personas';

	public $timestamps = false;

	//establecemos la relación uno a uno con
	//el modelo Licencia. este modelo es el padre
	public function licencia(){

		return $this->hasOne('Licencia', 'personas_id');
	}

	//Establecemos la relación de uno a uno con el
	//modelo hijo Auto.
	public function vehiculo(){

		return $this->hasOne('Auto', 'conductor');
	}

	//Establecemos la relación de uno a uno con el
	//modelo hijo Auto.
	public function propiedad(){

		return $this->hasOne('Auto', 'duenio');
	}

	//Establecemos la relación uno a uno con el modelo
	//padre Direccion
	public function direccion(){

		return $this->belongsTo('Direccion', 'direcciones_id');
	}

	public function usuario(){

		return $this->hasOne('Usuario', 'personas_id');
	}

	
	
}