@extends('administrador')

@section('titulo')
Apertura caja
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Aperturar Caja
        <small>{{Agencia::find(Auth::user()->agencias_id)->nombre}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Caja</a></li>
        <li class="active">Aperturar</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                {{Form::open(array('url'=>'cierre'))}}
                    <div class="box-body">
                        <label>Datos de Apertura</label>
                        <div class="form-group has-error">
                            <input type="text" name="inicio" class="form-control moneda" placeholder="Monto Inicial *" required>
                        </div>
                        <div class="form-group has-error">
                            <input type="password" name="contrasenia" class="form-control" placeholder="Contraseña *" required>
                        </div>
                        <div class="form-group">
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-black">Guardar</button>
                        <a href="<?=URL('administrador')?>" class="btn bg-maroon">Cancelar</a>
                    </div>
                {{Form::close()}}
            </div>

        </div>
    </div>
</section>
@stop

@section('scripts')

@stop