<!DOCTYPE html>
<html>
<head>
	<title>Cierre de Caja</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body  style="width:454px">
	<div>
		<h1>Tocache Express</h1>
		<h4>Cierre de Caja</h4>
	</div>
	<div>
		<table class="table table-hover">
            <tr>
                <th>Atributos</th>
                <th>Datos</th>
            </tr>
            <tr>
                <td>Cajero</td>
                <td>{{Persona::find(Auth::user()->personas_id)->nombre}}
                	{{Persona::find(Auth::user()->personas_id)->apellidos}}</td>
            </tr>
            <tr>
                <td>Agencia</td>
                <td>{{Agencia::find(Auth::user()->agencias_id)->nombre}}</td>
            </tr>
            <tr>
                <td>Monto Inicial</td>
                <td>S/. {{Cierre::find(Auth::user()->cierre)->inicio}}.00</td>
            </tr>
            <tr>
                <td>Movimiento del Día</td>
                <td>S/. {{Cierre::find(Auth::user()->cierre)->total}}.00</td>
            </tr>
            <tr>
                <td>Total en Caja</td>
                <td>S/. {{Cierre::find(Auth::user()->cierre)->inicio + Cierre::find(Auth::user()->cierre)->total}}.00</td>
            </tr>
            <tr>
                <td>Son</td>
                <td>{{$letras}}</td>
            </tr>
            <tr>
                <td>Cierre</td>
                <td>{{date('d-m-Y H:i', time()-18000)}}</td>
            </tr>
            <tr>
                <td>Firma</td>
                <td></td>
            </tr><br><br>
            <br>
            <br>
        </table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('administrador')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>