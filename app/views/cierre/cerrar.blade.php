@extends('administrador')

@section('titulo')
Cerrar caja
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Cerrar Caja
        <small>{{Agencia::find(Auth::user()->agencias_id)->nombre}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Caja</a></li>
        <li class="active">cerrar</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                {{Form::open(array('url'=>'cierre/'.Auth::user()->cierre))}}
                    <div class="box-body">
                        <label>Datos del Cierre</label>
                        <table class="table table-hover">
                        <tr>
                            <th>Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>Cajero</td>
                            <td>{{Persona::find(Auth::user()->personas_id)->nombre}}
                            	{{Persona::find(Auth::user()->personas_id)->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>Agencia</td>
                            <td>{{Agencia::find(Auth::user()->agencias_id)->nombre}}</td>
                        </tr>
                        <tr>
                            <td>Monto Inicial</td>
                            <td>S/. {{Cierre::find(Auth::user()->cierre)->inicio}}.00</td>
                        </tr>
                        <tr>
                            <td>Movimiento del Día</td>
                            <td>S/. {{Cierre::find(Auth::user()->cierre)->total}}.00</td>
                        </tr>
                        <tr>
                            <td>Total en Caja</td>
                            <td>S/. {{Cierre::find(Auth::user()->cierre)->inicio + Cierre::find(Auth::user()->cierre)->total}}.00</td>
                        </tr>
                        <tr>
                            <td>Cierre</td>
                            <td>{{date('d-m-Y H:i', time()-18000)}}</td>
                        </tr>
                    </table>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-black">Cerrar Caja</button>
                        <a href="<?=URL('administrador')?>" class="btn bg-maroon">Cancelar</a>
                    </div>
                    {{Form::hidden('_method', 'put')}}
                {{Form::close()}}
            </div>

        </div>
    </div>
</section>
@stop