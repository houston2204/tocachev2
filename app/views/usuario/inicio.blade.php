@extends('administrador')

@section('titulo')
Lista Usuarios
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Lista de Usuarios
        <small>usuarios de la agencia de {{Agencia::find(Auth::user()->agencias_id)->nombre}} </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuario</a></li>
        <li class="active">Lista de usuarios</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-8">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>DNI</th>
                                <th>Usuario</th>
                                <th>Agencia</th>
                                <th>Nivel</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Borrar</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{Persona::find($usuario->personas_id)->dni}} </td>
                                <td>{{Persona::find($usuario->personas_id)->nombre}} 
                                {{Persona::find($usuario->personas_id)->apellidos}}</td>
                                <td>{{Agencia::find($usuario->agencias_id)->nombre}}</td>
                                <td>
                                    @if($usuario->tipo)
                                        ADMINISTRADOR
                                    @else
                                        USUARIO
                                    @endif
                                </td>
                                <td><a href="<?=URL::to('usuario/'.$usuario->id)?>" class="label label-success">Ver</a></td>
                                <td><a href="<?=URL::to('usuario/'.$usuario->id.'/edit')?>" class="label label-info">Editar</a></td>
                                <td>
                                	{{ Form::open(array('url' => 'usuario/'.$usuario->id)) }}
                                    {{ Form::hidden("_method", "DELETE") }}
                                    	<input type="submit" class="btn btn-danger btn-xs" value="Borrar">
                                    {{ Form::close() }}</td>
                            	</tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>DNI</th>
                                <th>Usuario</th>
                                <th>Agencia</th>
                                <th>Nivel</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Borrar</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('scripts')
<!-- DATA TABES SCRIPT -->
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop