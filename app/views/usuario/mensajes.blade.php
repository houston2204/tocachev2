@extends('administrador')

@section('titulo')
Cambiar Contraseña
@stop

@section('contenido')
<section class="content-header">
    <h1>
        {{Persona::find(Auth::user()->personas_id)->nombre}} {{Persona::find(Auth::user()->personas_id)->apellidos}}
        <small>Resultado</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=URL::to('inicio')?>"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="<?=URL::to('usuario')?>">Personal</a></li>
        <li class="active">Resultado</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Resultado de Operaciones</h3>
                </div>
                <div class="box-body table-responsive">
                	{{$resultado}}
                </div>
            </div>
        </div>
    </div>
</section>
@stop