@extends('administrador')

@section('titulo')
Mostrar Usuario
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos del Usuario
        <small>Datos personales</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuario</a></li>
        <li class="active">Datos</li>
    </ol>
</section>

<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-6">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td>{{Persona::find($usuario->personas_id)->dni}}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{{Persona::find($usuario->personas_id)->nombre}}</td>
                        </tr>
                        <tr>
                            <td>Apellidos</td>
                            <td>{{Persona::find($usuario->personas_id)->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{{Persona::find($usuario->personas_id)->telefono}}</td>
                        </tr>
                        <tr>
                            <td>Dirección</td>
                            <td>{{Direccion::find(Persona::find($usuario->personas_id)->direcciones_id)->lugar}}  
                            	{{Direccion::find(Persona::find($usuario->personas_id)->direcciones_id)->distrito}}  
                            	{{Direccion::find(Persona::find($usuario->personas_id)->direcciones_id)->provincia}}  
                            	{{Direccion::find(Persona::find($usuario->personas_id)->direcciones_id)->departamento}}
                            </td>
                        </tr>
                        <tr>
                            <td>Agencia</td>
                            <td>{{Agencia::find($usuario->agencias_id)->nombre}}</td>
                        </tr>
                        <tr>
                            <td>Nivel</td>
                            <td>
                            	@if($usuario->tipo)
                            		ADMINISTRADOR
                            	@else
                            		USUARIO
                            	@endif
                            </td>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <a href="<?=URL::to('usuario')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('usuario/'.$usuario->id.'/edit')?>" class="btn btn-info">Editar</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop