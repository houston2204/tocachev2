@extends('administrador')

@section('titulo')
Cambiar Contraseña
@stop

@section('contenido')
<section class="content-header">
    <h1>
        {{Persona::find(Auth::user()->personas_id)->nombre}} {{Persona::find(Auth::user()->personas_id)->apellidos}}
        <small>Cambiar Contraseña</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#">Personal</a></li>
        <li class="active">Cambiar Contraseña</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ingresar Datos del Usuario</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {{Form::open(array('url'=>'contrasenia/cambiar'))}}
                    <div class="box-body">
                    <table>
                        <tbody>
                            <tr>
                                <div class="form-group has-error">
                                    <input type="text" class="form-control" name="dni" placeholder="Ingrese su DNI *" required
                                    autofocus>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group has-error">
                                    <input type="password" class="form-control" name="password" placeholder="contraseña *" 
                                    required>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group has-error">
                                    <input type="password" class="form-control" name="newpassword1" 
                                    placeholder="nueva contraseña *" required>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group has-error">
                                    <input type="password" class="form-control" name="newpassword2" 
                                    placeholder="confirmar nueva contraseña *" required>
                                </div>
                            </tr>
                            <tr>
                        </tbody>
                    </table>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                {{Form::close()}}
            </div><!-- /.box -->
        </div><!--/.col (left) -->
    </div>   <!-- /.row -->
</section><!-- /.content -->
@stop