@extends('administrador')

@section('titulo')
Usuario
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        @if($usuario->id)
        Actualizar 
        @else
        Ingresar 
        @endif
        Datos
        <small>
            @if($usuario->id)
            Actualizar 
            @else
            Nuevo 
            @endif
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuario</a></li>
        <li class="active">
            @if($usuario->id)
            Actualizar 
            @else
            Nuevo 
            @endif
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box box-success">
                {{Form::open(array('url'=>'usuario/'.$usuario->id))}}
                    <div class="box-body">
                        @if($usuario->id)
                        <label for="exampleInputEmail1">Datos del usuario con DNI {{$usuario->email}}</label>
                        @else
                        <label for="exampleInputEmail1">Datos del usuario</label>
                        <div class="form-group has-error">
                            <input type="text" name="dni" class="form-control dni" placeholder="DNI *" 
                            value="{{$persona->dni}}" required autofocus>
                        </div>
                        @endif
                        <div class="form-group has-error">
                            <input type="text" name="nombre" class="form-control mayusculas" placeholder="Nombre *" 
                            value="{{$persona->nombre}}" required autofocus>
                        </div>
                        <div class="form-group has-error">
                            <input type="text" name="apellidos" class="form-control mayusculas" placeholder="Apellidos *" 
                            value="{{$persona->apellidos}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="telefono" class="form-control telefono mayusculas" placeholder="Teléfono"
                            value="{{$persona->telefono}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="lugar" class="form-control mayusculas" 
                            placeholder="Dirección Av. Jr. Urb. AAHH. Res. ..." 
                            value="{{$direccion->lugar}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="distrito" class="form-control mayusculas" placeholder="Distrito" 
                            value="{{$direccion->distrito}}">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="provincia" class="form-control mayusculas" placeholder="Provincia" 
                            value="{{$direccion->provincia}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="departamento" class="form-control mayusculas" placeholder="Departamento" 
                            value="{{$direccion->departamento}}">
                        </div>
                        <div class="form-group">
                            <label>Agencia</label>
                            <select class="form-control" name="agencia">
                            @if($usuario->id)
                                <option value="{{$usuario->agencias_id}}">
                                    {{Agencia::find($usuario->agencias_id)->nombre}}
                                </option>
                            @endif
                            @foreach(Agencia::all() as $agencia)
                                <option value="{{$agencia->id}}">{{$agencia->nombre}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                @if($usuario->tipo)
                                    <input type="checkbox" name="tipo" value="1" checked />
                                    ADMINISTRADOR
                                @else
                                    <input type="checkbox" name="tipo" value="1" />
                                    ADMINISTRADOR
                                @endif
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-black">Guardar</button>
                        <a href="<?=URL('usuario')?>" class="btn bg-maroon">Cancelar</a>
                    </div>
                    @if($usuario->id)
                        {{Form::hidden('_method', 'put')}}
                    @endif
                {{Form::close()}}
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script type="text/javascript">
    $(".mayuscula").keyup(function(){
        $(".mayuscula").val().toLowerCase;
    })
</script>
@stop