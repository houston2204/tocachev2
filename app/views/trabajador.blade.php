<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>@yield('titulo') | Tocache Express</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="Shortcut Icon" href="<?=URL::to('favicon.ico')?>" type="image/x-icon" />
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" 
        type="text/css" />
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
        @yield('estilos')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <header class="header bg-green">
            <a href="<?=URL::to('administrador')?>" class="logo bg-green">
                <img src="<?=URL::to('img/logo.jpg')?>" class="logo icon bg-green" alt="Tocache express" />
                
            </a>
            <nav class="navbar navbar-static-top bg-green" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle bg-green" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">{{$notificaciones}}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Tiene {{$notificaciones}} Notificaciones</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users danger"></i>{{$lv}} Licencias estan Vencidas
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-users warning"></i>
                                                {{$lw}} conductores tiene la licencia por vencer
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-dashboard danger"></i>{{$sv}} SOAT's estan Vencidos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-dashboard warning"></i>{{$sw}} autos tiene el SOAT por vencer
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer"><a href="#">Ver todos</a></li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu bg-green">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{Persona::find(Auth::user()->personas_id)->nombre }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu bg-green">
                                <!-- User image -->
                                <li class="user-header bg-green">
                                    <img src="<?=URL::to('img/avatar3.png')?>" class="img-circle" alt="User Image" />
                                    <p>
                                        {{Persona::find(Auth::user()->personas_id)->nombre }} - Administrador
                                        <small>Encargado de agencia en {{Agencia::find(Auth::user()->agencias_id)->nombre}} 
                                        </small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?=URL::to('contrasenia')?>" class="btn btn-default btn-flat">
                                        Cambiar Contraseña</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?=URL::to('salir')?>" class="btn btn-default btn-flat">Salir</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left bg-red text-black">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?=URL::to('img/avatar3.png')?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hola {{Persona::find(Auth::user()->personas_id)->nombre }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Conectado</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?=URL::to('administrador')?>">
                                <i class="fa fa-home"></i> <span>Inicio</span>
                            </a>
                        </li>
                        @if(Auth::user()->tipo)
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Agencias</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('agencia/create')?>"><i class="fa fa-angle-double-right"></i> 
                                Nuevo</a></li>
                                <li><a href="<?=URL::to('agencia')?>"><i class="fa fa-angle-double-right"></i> Todos</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-user"></i>
                                <span>Usuarios</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('usuario/create')?>"><i class="fa fa-angle-double-right"></i> 
                                Nuevo</a></li>
                                <li><a href="<?=URL::to('usuario')?>"><i class="fa fa-angle-double-right"></i> Todos</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-male"></i>
                                <span>Conductores *</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('conductor/create')?>"><i class="fa fa-angle-double-right"></i> 
                                Nuevo</a></li>
                                <li><a href="<?=URL::to('conductor')?>"><i class="fa fa-angle-double-right"></i> Todos</a>
                                </li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Imprimir Nómina *</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i>
                                <span>Autos *</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('auto/create')?>"><i class="fa fa-angle-double-right"></i> 
                                Nuevo</a></li>
                                <li><a href="<?=URL::to('auto')?>"><i class="fa fa-angle-double-right"></i> Todos</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Imprimir Padrón *</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i> <span>Reportes *</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Llegadas y Salidas *</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Pasajes *</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Encomiendas *</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Telegiros *</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right"></i> Total *</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?=URL::to('buscar/usuarios')?>">
                                <i class="fa fa-book"></i>
                                <span>Abrir Cajas</span>
                            </a>
                        </li>
                        @endif
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-sign-in"></i>
                                <span>Llegadas y Salidas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('llegada')?>"><i class="fa fa-angle-double-right"></i> Inicio</a></li>
                                <li><a href="<?=URL::to('buscar/conductor')?>"><i class="fa fa-angle-double-right"></i>
                                Nuevo</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-road"></i>
                                <span>Pasajes</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('pasaje')?>"><i class="fa fa-angle-double-right"></i> Todos</a></li>
                                <li><a href="<?=URL::to('buscar/cliente')?>"><i class="fa fa-angle-double-right"></i> 
                                Vender</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-truck"></i>
                                <span>Encomiendas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('encomienda')?>"><i class="fa fa-angle-double-right"></i> Todos</a>
                                </li>
                                <li><a href="<?=URL::to('buscar/remitente')?>"><i class="fa fa-angle-double-right"></i> 
                                Enviar</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span>Telegiros</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('giro')?>"><i class="fa fa-angle-double-right"></i> Todos</a></li>
                                <li><a href="<?=URL::to('buscar/deposito')?>"><i class="fa fa-angle-double-right"></i> 
                                Enviar</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Caja</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?=URL::to('buscar/apertura')?>"><i class="fa fa-angle-double-right"></i> 
                                Aperturar</a></li>
                                <li><a href="<?=URL::to('buscar/cerrar')?>"><i class="fa fa-angle-double-right"></i> 
                                Cerrar</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>

            <aside class="right-side bg-red text-black">
                @yield('contenido')
            </aside>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=URL::to('js/AdminLTE/app.js')?>" type="text/javascript"></script>
        <script src="<?=URL::to('js/AdminLTE/demo.js')?>" type="text/javascript"></script>
        @yield('scripts')
    </body>
</html>
