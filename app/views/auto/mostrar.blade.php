@extends('administrador')

@section('titulo')
Mostrar Auto
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos del Auto
        <small>Características</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">Datos</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-5">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 200px;">Atributos</th>
                            <th>Caracteristicas</th>
                        </tr>
                        <tr>
                            <td>Placa</td>
                            <td>{{$auto->placa}}</td>
                        </tr>
                        <tr>
                            <td>Marca</td>
                            <td>{{$auto->marca}}  </td>
                        </tr>
                        <tr>
                            <td>Modelo</td>
                            <td>{{$auto->modelo}}</td>
                        </tr>
                        <tr>
                            <td>Color</td>
                            <td>{{$auto->color}}</td>
                        </tr>
                        <tr>
                            <td>Dueño</td>
                            <td>
                            	{{Persona::find($auto->duenio)->nombre}}  
                            	{{Persona::find($auto->duenio)->apellidos}} 
                            </td>
                        </tr>
                        <tr>
                            <td>DNI/RUC</td>
                            <td>{{Persona::find($auto->duenio)->dni}}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{{Persona::find($auto->duenio)->telefono}}</td>
                        </tr>
                        <tr>
                            <td>Dirección</td>
                            <td>
                            	{{Direccion::find(Persona::find($auto->duenio)->direcciones_id)->lugar}}
                            	{{Direccion::find(Persona::find($auto->duenio)->direcciones_id)->distrito}}
                            	{{Direccion::find(Persona::find($auto->duenio)->direcciones_id)->provincia}}
                            	{{Direccion::find(Persona::find($auto->duenio)->direcciones_id)->departamento}}
                            </td>
                        </tr>
                        <tr>
                            <td>SOAT</td>
                            <td>{{$auto->seguro->numero}}</td>
                        </tr>
                        <tr>
                            <td>Vencimiento</td>
                            <td>{{date('d-m-Y', strtotime($auto->seguro->vencimiento))}}</td>
                        </tr>
                        <tr>
                            <td>Conductor</td>
                            <td>
                            	{{Persona::find($auto->conductor)->nombre}}  
                            	{{Persona::find($auto->conductor)->apellidos}} 
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="<?=URL::to('auto')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('auto/'.$auto->id.'/edit')?>" class="btn btn-info">Editar</a>
            <a href="<?=URL::to('conductor/'.$auto->chofer->id)?>" class="btn btn-warning">Ver Conductor</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop