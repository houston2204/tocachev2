@extends('administrador')

@section('titulo')
Panel de Control
@stop

@section('estilos')

@stop

@section('contenido')
<!-- Encabezado de la página -->
<section class="content-header">
    <h1>
        Inicio
        <small>Panel de control</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Panel de control</li>
    </ol>
</section>

<!-- Main content -->
<section class="content ">
    <h4 class="page-header">
        Panel de Control
        <small>Bienvenido al sistemas de control de Tocache Express. Comience a administrar su sistema.</small>
    </h4>
    <div class="row">
        <div class="col-md-12">
        @if(Session::has('rojo'))
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alerta!</b> {{ Session::get('rojo')}}
            </div>
        @elseif(Session::has('verde'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Exelente!</b> {{ Session::get('verde')}}
            </div>
        @elseif(Session::has('naranja'))
            <div class="alert alert-warning alert-dismissable">
                <i class="fa fa-warning"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Cuidado!</b> {{ Session::get('naranja')}}
            </div>
        @endif
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Usuarios
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?=URL::to('usuario')?>" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Conductores
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?=URL::to('conductor')?>" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Autos
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-pricetag-outline"></i>
                </div>
                <a href="<?=URL::to('auto')?>" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        %
                    </h3>
                    <p>
                        Reportes
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        </div>
    </div><!-- /.row -->

</section><!-- /.content -->
@stop

@section('scripts')

@stop