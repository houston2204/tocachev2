@extends('administrador')

@section('titulo')
Pasajeros
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Pasajeros y Encomiendas
        <small>Agregar al auto {{$llegada->auto}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Pasajeros</a></li>
        <li class="active">agregar al auto</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        {{Form::open(array('url'=>'llegada/'.$llegada->id))}}
        <div class="col-md-10">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <h4>ENCOMIENDAS</h4>        
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Remitente</th>
                                <th>Paquete</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Llevar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pasajes as $pasaje)
                            @if($pasaje->estado == 1 && $pasaje->servicios_id == 3)
                            <tr>
                                <td>{{Persona::find($pasaje->cliente)->nombre}} 
                                {{Persona::find($pasaje->cliente)->apellidos}}</td>
                                <td>{{$pasaje->descripcion}}</td>
                                <td>{{$pasaje->destino}}</td>
                                <td>
                                    @if($pasaje->estado)
                                    <a href="#" class="label label-warning">Esperando</a>
                                    @else
                                    {{date("h:i:s A", strtotime($pasaje->updated_at)-18000)}}
                                    @endif
                                </td>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="pasajero{{$pasaje->id}}" value="{{$pasaje->id}}" />
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Remitente</th>
                                <th>Paquete</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Llevar</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="box">
                <div class="box-body table-responsive">
                    <h4>PASAJEROS</h4> 
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Pasajero</th>
                                <th>DNI</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Llevar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pasajes as $pasaje)
                            @if($pasaje->estado == 1 && $pasaje->servicios_id == 2)
                            <tr>
                                <td>{{Persona::find($pasaje->cliente)->nombre}} 
                                {{Persona::find($pasaje->cliente)->apellidos}}</td>
                                <td>{{Persona::find($pasaje->cliente)->dni}}</td>
                                <td>{{$pasaje->destino}}</td>
                                <td>
                                    @if($pasaje->estado)
                                    <a href="#" class="label label-warning">Esperando</a>
                                    @else
                                    {{date("h:i:s A", strtotime($pasaje->updated_at)-18000)}}
                                    @endif
                                </td>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="pasajero{{$pasaje->id}}" value="{{$pasaje->id}}" />
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Pasajero</th>
                                <th>DNI</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Llevar</th>
                            </tr>
                        </tfoot>
                    </table>
                    @if($llegada->destino != null)
                    Destino: <input class="mayusculas" type="text" name="destino" value="{{$llegada->destino}}" readonly="">
                    @else
                    <div class="form-group has-error">
                    Destino: <input class="mayusculas" type="text" name="destino" value="" required>
                    </div>
                    @endif
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <button type="submit" class="btn btn-primary bg-black">Llevar</button>
            <a href="<?=URL::to('llegada')?>" class="btn bg-maroon">Cancelar</a>
        </div>
        @if($llegada->id)
            {{Form::hidden('_method', 'put')}}
        @endif
        {{Form::close()}}
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
    });
</script>
@stop