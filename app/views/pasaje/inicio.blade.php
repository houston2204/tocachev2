@extends('administrador')

@section('titulo')
Pasajes 
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Pasajes
        <small>lista del día</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Pasajes</a></li>
        <li class="active">Pasajes de Hoy {{$hoy}}</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-11">
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Pasajero</th>
                                <th>DNI</th>
                                <th>Salida</th>
                                <th>Destino</th>
                                <th>Estado</th>
                                <th>Editar</th>
                                <th>Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pasajes as $pasaje)
                            @if(strtotime($pasaje->fecha) >= $hoy)
                            <tr>
                                <td>{{Persona::find($pasaje->cliente)->nombre}} {{Persona::find($pasaje->cliente)->apellidos}}</td>
                                <td>{{Persona::find($pasaje->cliente)->dni}}</td>
                                <td>{{$pasaje->salida}}</td>
                                <td>{{$pasaje->destino}}</td>
                                <td>
                                    @if($pasaje->estado)
                                    <a href="#" class="btn btn-warning btn-xs">Esperando</a>
                                    @else
                                    {{date("h:i:s A", strtotime($pasaje->updated_at)-18000)}}
                                    @endif
                                </td>
                                <td>
                                @if($pasaje->agencia == Auth::user()->agencias_id)
                                    @if($pasaje->estado)
                                    <a href="<?=URL::to('pasaje/'.$pasaje->id.'/edit')?>" class="btn btn-info btn-xs">Editar</a>
                                    @else
                                    Salio
                                    @endif
                                @else
                                    @if($pasaje->estado)
                                    No Autorizado
                                    @else
                                    Salio
                                    @endif
                                @endif
                                </td>
                                <td><a href="<?=URL::to('pasaje/'.$pasaje->id)?>" class="btn btn-success btn-xs">Ver</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Pasajero</th>
                                <th>DNI</th>
                                <th>Salida</th>
                                <th>Destino</th>
                                <th>Estado</th>
                                <th>Editar</th>
                                <th>Ver</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop