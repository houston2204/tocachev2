@extends('administrador')

@section('titulo')
Pasaje
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos del pasaje
        <small>Datos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Servicios</a></li>
        <li class="active">Pasaje</li>
    </ol>
</section>

<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-5">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 200px;">Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>USUARIO</td>
                            <td>{{$pasaje->usuario}}</td>
                        </tr>
                        <tr>
                            <td>RUC</td>
                            <td>{{$pasaje->ruc}}</td>
                        </tr>
                        <tr>
                            <td>Pasajero</td>
                            <td>{{Persona::find($pasaje->cliente)->nombre}}
                            	{{Persona::find($pasaje->cliente)->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td>{{Persona::find($pasaje->cliente)->dni}}</td>
                        </tr>
                        <tr>
                            <td>Día</td>
                            <td>{{date('d-m-Y', strtotime($pasaje->fecha))}}</td>
                        </tr>
                        <tr>
                            <td>Hora</td>
                            <td>{{date("h:i:s A", strtotime($pasaje->fecha))}}</td>
                        </tr>
                        <tr>
                            <td>Salida</td>
                            <td>{{$pasaje->salida}}</td>
                        </tr>
                        <tr>
                            <td>Destino</td>
                            <td>{{$pasaje->destino}}</td>
                        </tr>
                        <tr>
                            <td>Conductor</td>
                            <td>{{$pasaje->conductor}}</td>
                        </tr>
                        <tr>
                            <td>Auto</td>
                            <td>{{$pasaje->auto}}</td>
                        </tr>
                        <tr>
                            <td>Costo</td>
                            <td>S/. {{$pasaje->costo}}.00</td>
                        </tr>
                        <tr>
                            <td>Cajero</td>
                            <td>{{$pasaje->cajero}}</td>
                        </tr>
                        <tr>
                            <td>Salida</td>
                            <td>
                            @if($pasaje->estado)
                            	En Espera
                            @else
                            	{{date("h:i:s A", strtotime($pasaje->updated_at)-18000)}}
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="<?=URL::to('pasaje')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('#')?>" class="btn btn-warning">Recibo</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop