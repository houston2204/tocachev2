@extends('administrador')

@section('titulo')
Vender Pasaje
@stop

@section('estilos')
<link href="<?=URL::to('css/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=URL::to('css/iCheck/all.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=URL::to('css/colorpicker/bootstrap-colorpicker.min.css')?>" rel="stylesheet"/>
<link href="<?=URL::to('css/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet"/>
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Ingresar Datos
        <small>
        Vender Pasaje
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Pasaje</a></li>
        <li class="active">
            Vender
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-body">
                        <label>Pasaje</label>
                        <div class="row">
                        	<div class="col-md-8">
                        		<div class="form-group">
                        			<input type="text" name="usuario" class="form-control" placeholder="Razón Social" readonly="">
                        		</div>
                        	</div>
                        	<div class="col-md-4">
                        		<div class="form-group">
                        			<input type="text" name="ruc" class="form-control" placeholder="RUC" readonly="">
                        		</div>
                        	</div>
                        </div>
                        {{Form::open(array('url'=>'buscar/buscar-cliente'))}}
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="nombre" class="form-control" placeholder="Nombre" readonly="">
                        		</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="apellidos" class="form-control" placeholder="Apellidos" readonly="">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="dni" class="form-control dni" placeholder="DNI *" required>
                        		</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="destino" class="form-control" placeholder="Destino" readonly="">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group has-error">
                                    <input type="text" name="salida" class="form-control" placeholder="Recojer (solo a domicilio)" readonly="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group has-error">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                        {{Form::close()}}
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error input-group">
                        			<span class="input-group-addon">Fecha</span>
                        			<input type="text" name="fecha" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'"
                        			data-mask readonly=""/>
                        		</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="bootstrap-timepicker">
	                        		<div class="form-group has-error input-group">
	                        			<span class="input-group-addon">Hora</span>
	                        			<input type="text" name="hora" class="form-control timepicker" readonly=""/>
	                        		</div>
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6 right">
                        		<div class="form-group has-error input-group">
                        			<input type="text" name="costo" class="form-control" placeholder="Costo" readonly="">
                        		</div>
                        	</div>
                        </div>
	                </div>
	            </div>
	        </div>        
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.date.extensions.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.extensions.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/daterangepicker/daterangepicker.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/colorpicker/bootstrap-colorpicker.min.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/timepicker/bootstrap-timepicker.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    startDate: moment().subtract('days', 29),
                    endDate: moment()
                },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>
@stop