<!DOCTYPE html>
<html>
<head>
	<title>Pasaje</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
    <style type="text/css">
    td {
    	font-size: 12px;
    }
    </style>
</head>
<body  style="width:545px">
	<div>
		<h1>Tocache Express</h1>
	</div>
	<div>
		<table class="table" border="0">
			<tr>
				<td>USUARIO:</td>
				<td width="400px">{{$pasaje->usuario}}</td>
				<td>R.U.C.:</td>
				<td width="145px">{{$pasaje->ruc}}</td>
			</tr>
			<tr>
				<td>PASAJERO:</td>
				<td>{{Persona::find($pasaje->cliente)->nombre}}
				{{Persona::find($pasaje->cliente)->apellidos}}</td>
				<td>DNI:</td>
				<td>{{Persona::find($pasaje->cliente)->dni}}</td>
			</tr>
			<tr>
				<td>SALIDA:</td>
				<td>{{$pasaje->salida}}</td>
				<td>DESTINO:</td>
				<td>{{$pasaje->destino}}</td>
			</tr>
			<tr>
				<td>VALOR:</td>
				<td width="145px">S/. {{$pasaje->costo}}</td>
				<td>Son:</td>
				<td width="400px">{{$letras}}</td>
			</tr>
			<tr>
				<td>FECHA DE VIAJE</td>
				<td>{{date('d-m-Y', strtotime($pasaje->fecha))}}</td>
				<td>HORA DE VIAJE</td>
				<td>{{date("h:i A", strtotime($pasaje->fecha))}}</td>
			</tr>
			<tr>
				<td>Tocache Express</td>
				<td>{{$pasaje->cajero}}</td>
				<td>Fecha de Emisión</td>
				<td>{{date('d-m-Y', strtotime($pasaje->created_at)-18000)}}</td>
			</tr>
			<tr>
				<td colspan="4">
					Es obligatorio presentarse media hora antes de la partida, portando su D.N.I. Original (adultos y niños)
				</td>
			</tr>
		</table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('pasaje')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>