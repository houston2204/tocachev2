@extends('administrador')

@section('titulo')
Mostrar Conductor
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos del Conductor
        <small>Datos personales</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">Datos</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-5">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 200px;">Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td>{{$conductor->dni}}</td>
                        </tr>
                        <tr>
                            <td>Código</td>
                            <td>{{$conductor->licencia->codigo}}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{{$conductor->nombre}}</td>
                        </tr>
                        <tr>
                            <td>Apellidos</td>
                            <td>{{$conductor->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>Teléfono</td>
                            <td>{{$conductor->telefono}}</td>
                        </tr>
                        <tr>
                            <td>Dirección</td>
                            <td>{{Direccion::find($conductor->direcciones_id)->lugar}}  
                            	{{Direccion::find($conductor->direcciones_id)->distrito}}  
                            	{{Direccion::find($conductor->direcciones_id)->provincia}}  
                            	{{Direccion::find($conductor->direcciones_id)->departamento}}
                            </td>
                        </tr>
                        <tr>
                            <td>Nº Licencia</td>
                            <td>{{$conductor->licencia->numero}}</td>
                        </tr>
                        <tr>
                            <td>Categoría</td>
                            <td>{{$conductor->licencia->categoria}}</td>
                        </tr>
                        <tr>
                            <td>Vencimiento de Licencia</td>
                            <td>{{date('d-m-Y', strtotime($conductor->licencia->vencimiento))}}</td>
                        </tr>
                        <tr>
                            <td>Auto</td>
                            <td>{{$conductor->vehiculo->placa}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="<?=URL::to('conductor')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('conductor/'.$conductor->id.'/edit')?>" class="btn btn-info">Editar</a>
            <a href="<?=URL::to('auto/'.$conductor->vehiculo->id)?>" class="btn btn-warning">Ver Auto</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop