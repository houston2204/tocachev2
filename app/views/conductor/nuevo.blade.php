@extends('administrador')

@section('titulo')
@if($conductor->id)
Actualizar
@else
Nuevo
@endif
Conductor
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        @if($conductor->id)
        Actualizar
        @else
        Ingresar
        @endif
        Datos
        <small>
        @if($conductor->id)
        Actualizar
        @else
        Nuevo
        @endif
        conductor
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">
            @if($conductor->id)
            Actualizar
            @else
            Nuevo
            @endif
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        {{Form::open(array('url'=>'conductor/'.$conductor->id))}}
	        <div class="col-md-6">
                @if(Session::has('rojo'))
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Alerta!</b> {{ Session::get('rojo')}}
                    </div>
                @elseif(Session::has('verde'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Exelente!</b> {{ Session::get('verde')}}
                    </div>
                @elseif(Session::has('naranja'))
                    <div class="alert alert-warning alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Cuidado!</b> {{ Session::get('naranja')}}
                    </div>
                @endif
	            <div class="box box-success">
	                <div class="box-body">
                        @if($conductor->id)
                        <label style="font-size: 18px">Actualizar Conductor con DNI {{$conductor->dni}}</label>
                        @else
	                    <label>Datos del Conductor</label>
                            <div class="form-group has-error">
	                        <input type="text" name="dniconductor" class="form-control dni" placeholder="DNI *" 
	                        value="{{$conductor->dni}}" required autofocus>
	                    </div>
                        @endif
                        <div class="form-group has-error">
	                        <input type="text" name="codigo" class="form-control mayusculas" placeholder="Codigo *" 
	                        value="{{$licencia->codigo}}" required autofocus>
	                    </div>
	                    <div class="form-group has-error">
	                        <input type="text" name="nombreconductor" class="form-control mayusculas" placeholder="Nombre *" 
	                        value="{{$conductor->nombre}}" required>
	                    </div>
	                    <div class="form-group has-error">
	                        <input type="text" name="apellidosconductor" class="form-control mayusculas"
                            placeholder="Apellidos *" value="{{$conductor->apellidos}}" required>
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="telefonoconductor" class="form-control mayusculas telefono"
                            placeholder="Teléfono" value="{{$conductor->telefono}}">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="lugarconductor" class="form-control mayusculas" 
                            placeholder="Dirección Av. Jr. Urb. AAHH. Res. ..." value="{{$direccioncond->lugar}}">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="distritoconductor" class="form-control mayusculas" placeholder="Distrito" 
	                        value="{{$direccioncond->distrito}}">
	                    </div>
                        <div class="form-group">
                            <input type="text" name="provinciaconductor" class="form-control mayusculas" placeholder="Provincia" 
                            value="{{$direccioncond->provincia}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="departamentoconductor" class="form-control mayusculas"
                            placeholder="Departamento" value="{{$direccioncond->departamento}}">
                        </div>
	                    <div class="form-group has-error">
	                        <input type="text" name="numlicencia" class="form-control mayusculas licencia" placeholder="Licencia *" 
	                        value="{{$licencia->numero}}" required>
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="categoria" class="form-control mayusculas" placeholder="Categoria"
                            value="{{$licencia->categoria}}">
	                    </div>
	                    <div class="form-group has-error">
	                        <div class="input-group">
	                            <div class="input-group-addon">
	                                Vencimiento: *
	                            </div>
			                    @if($conductor->id)
		                        <input type="text" name="venclicencia" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" 
		                            data-mask value="{{date('d-m-Y', strtotime($licencia->vencimiento))}}" required/>
			                    @else
		                    	<input type="text" name="venclicencia" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" 
		                            data-mask value="" required/>
			                    @endif
	                        </div>
	                    </div>

		                
	                </div>
	            </div>
	        </div>
	        <div class="col-md-6">
	            <div class="box box-success">
                    <div class="box-body">
                        <label>Datos del Auto</label>
                        <div class="form-group has-error">
                            <input type="text" name="placa" class="form-control mayusculas" placeholder="Número de placa *" 
                            value="{{$auto->placa}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="marca" class="form-control mayusculas" placeholder="Marca" 
                            value="{{$auto->marca}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="modelo" class="form-control mayusculas" placeholder="Modelo" 
                            value="{{$auto->modelo}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="color" class="form-control mayusculas" placeholder="Color" 
                            value="{{$auto->color}}">
                        </div>
                        <div class="form-group has-error">
                            <input type="text" name="seguro" class="form-control mayusculas" placeholder="SOAT *" 
                            value="{{$seguro->numero}}" required>
                        </div>
                        <div class="form-group has-error">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Vencimiento: *
                                </div>
                                @if($conductor->id)
                                <input type="text" name="vencseguro" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" 
                                data-mask value="{{date('d-m-Y', strtotime($seguro->vencimiento))}}" required/>
                                @else
                                <input type="text" name="vencseguro" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" 
                                data-mask value="" required/>
                                @endif
                            </div>
                        </div>
                        <label>Datos del Dueño</label>
                        <div class="form-group has-error">
                            <input type="text" name="dniduenio" class="form-control dniruc" placeholder="DNI / RUC *" 
                            value="{{$duenio->dni}}" required>
                        </div>
                        <div class="form-group has-error">
                        	<input type="text" name="nombreduenio" class="form-control mayusculas" placeholder="Nombre *" 
                            value="{{$duenio->nombre}}" required>
                        </div>
                        <div class="form-group has-error">
                            <input type="text" name="apellidosduenio" class="form-control mayusculas" placeholder="Apellidos *" 
                            value="{{$duenio->apellidos}}" required>
                        </div>
                         <div class="form-group">
                            <input type="text" name="telefonoduenio" class="form-control telefono" placeholder="Teléfono" 
                            value="{{$duenio->telefono}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="lugarduenio" class="form-control mayusculas" placeholder="Dirección Av. Jr. Urb. AAHH. Res. ..." 
                            value="{{$direccionduen->lugar}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="distritoduenio" class="form-control mayusculas" placeholder="Distrito" 
                            value="{{$direccionduen->distrito}}">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="provinciaduenio" class="form-control mayusculas" placeholder="Provincia" 
                            value="{{$direccionduen->provincia}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="departamentoduenio" class="form-control mayusculas" placeholder="Departamento" 
                            value="{{$direccioncond->departamento}}">
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a href="<?=URL::to('conductor')?>" class="btn bg-maroon">Cancelar</a>
                        </div>
                    </div>
	            </div>
	        </div>
                
            @if($conductor->id)
                {{Form::hidden('_method', 'put')}}
            @endif
        {{Form::close()}}
    </div>
</section>
@stop

@section('scripts')
<!-- InputMask -->
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.date.extensions.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.extensions.js')?>" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="<?=URL::to('js/plugins/daterangepicker/daterangepicker.js')?>" type="text/javascript"></script>
<!-- bootstrap color picker -->
<script src="<?=URL::to('js/plugins/colorpicker/bootstrap-colorpicker.min.js')?>" type="text/javascript"></script>
<!-- bootstrap time picker -->
<script src="<?=URL::to('js/plugins/timepicker/bootstrap-timepicker.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    startDate: moment().subtract('days', 29),
                    endDate: moment()
                },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>
@stop