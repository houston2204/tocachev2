@extends('administrador')

@section('titulo')
Conductores
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Conductores
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">lista de conductores</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-10">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Licencia</th>
                                <th>Conductor</th>
                                <th>Auto</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Retirar</th>
                                <th>Auto</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($conductores as $conductor)
                            <tr>
                            
                            	<td>{{Licencia::find($conductor->idlicencia)->codigo}}</td>
                                <td>{{Licencia::find($conductor->idlicencia)->numero}}</td>
                                <td>{{$conductor->nombre}} {{$conductor->apellidos}}</td>
                                <td>{{$conductor->placa}}</td>
                                <td><a href="<?=URL::to('conductor/'.$conductor->idpersona)?>" class="btn btn-success btn-xs">Ver</a></td>
                                <td><a href="<?=URL::to('conductor/'.$conductor->idpersona.'/edit')?>" class="btn btn-info btn-xs">Editar</a></td>
                                <td>
                                    {{ Form::open(array('url' => 'conductor/'.$conductor->idpersona)) }}
                                    {{ Form::hidden("_method", "DELETE") }}
                                    <input type="submit" class="btn btn-danger btn-xs" value="Borrar">
                                    {{ Form::close() }}
                                </td>
                                <td><a href="<?=URL::to('auto/'.$conductor->idauto)?>" class="btn btn-warning btn-xs">Ver Auto</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                            	<th>Código</th>
                                <th>Licencia</th>
                                <th>Conductor</th>
                                <th>Auto</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Retirar</th>
                                <th>Auto</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

</section><!-- /.content -->
@stop

@section('scripts')
<!-- DATA TABES SCRIPT -->
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop