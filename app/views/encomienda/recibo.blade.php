<!DOCTYPE html>
<html>
<head>
	<title>Recibo encomienda</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body  style="width:650px">
	<div>
		<h1>Tocache Express</h1>
		<h4>Encomienda</h4>
	</div>
	<div>
		<table class="table" border="0">
			<tr>
				<td>USUARIO:</td>
				<td colspan="4">{{$encomienda->usuario}}</td>
				<td>R.U.C.:</td>
				<td colspan="3">{{$encomienda->ruc}}</td>
			</tr>
			<tr>
				<td>REMITENTE:</td>
				<td colspan="4">{{Persona::find($encomienda->cliente)->nombre}}
				{{Persona::find($encomienda->cliente)->apellidos}}</td>
				<td>DNI:</td>
				<td colspan="3">{{Persona::find($encomienda->cliente)->dni}}</td>
			</tr>
			<tr>
				<td>DESTINATARIO:</td>
				<td colspan="4">{{Persona::find($encomienda->receptor)->nombre}}
				{{Persona::find($encomienda->receptor)->apellidos}}</td>
				<td>DNI:</td>
				<td colspan="3">{{Persona::find($encomienda->receptor)->dni}}</td>
			</tr>
			<tr>
				<td>PROCEDENCIA:</td>
				<td colspan="3">{{$encomienda->salida}}</td>
				<td>DESTINO:</td>
				<td colspan="4">{{$encomienda->destino}}</td>
			</tr>
			<tr>
				<td>DESCRIPCION:</td>
				<td colspan="8">{{$encomienda->descripcion}}</td>
			</tr>
			<tr>
				<td>VALOR:</td>
				<td>S/. {{$encomienda->costo}}</td>
				<td>SON:</td>
				<td colspan="6">{{$letras}}</td>
			</tr>
			<tr>
				<td colspan="2">Tocache Express</td>
				<td colspan="2">{{$encomienda->cajero}}</td>
				<td colspan="2">Fecha de Emisión</td>
				<td>{{date('d', strtotime($encomienda->created_at)-18000)}}</td>
				<td>{{date('m', strtotime($encomienda->created_at)-18000)}}</td>
				<td>{{date('y', strtotime($encomienda->created_at)-18000)}}</td>
			</tr>
		</table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('administrador')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>