@extends('administrador')

@section('titulo')
Encomiendas 
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Encomiendas
        <small>Todas las Encomiendas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Encomiendas</a></li>
        <li class="active">Todos</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-10">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Remitente</th>
                                <th>Salida</th>
                                <th>Destinatario</th>
                                <th>Destino</th>
                                <th>Estado</th>
                                <th>Editar</th>
                                <th>Ver</th>
                                <th>Recoger</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($encomiendas as $encomienda)
                            <tr>
                                <td>
                                    {{Persona::find($encomienda->cliente)->nombre}} 
                                    {{Persona::find($encomienda->cliente)->apellidos}}
                                </td>
                                <td>{{$encomienda->salida}}</td>
                                <td>
                                    {{Persona::find($encomienda->receptor)->nombre}} 
                                    {{Persona::find($encomienda->receptor)->apellidos}}
                                </td>
                                <td>{{$encomienda->destino}}</td>
                                <td>
                                    @if($encomienda->estado != 1)
                                        {{date("h:i:s A", strtotime($encomienda->updated_at)-18000)}}
                                    @else
                                        <a href="#" class="label label-warning">Esperando</a>
                                    @endif
                                </td>
                                <td>
                                    @if($encomienda->agencia == Auth::user()->agencias_id)
                                        @if($encomienda->estado != 1)
                                            Salio
                                        @else
                                            <a href="<?=URL::to('encomienda/'.$encomienda->id.'/edit')?>" 
                                            class="label label-info">Editar</a>
                                        @endif
                                    @else
                                        @if($encomienda->estado != 1)
                                            Salio
                                        @else
                                            No Autorizado
                                        @endif
                                    @endif
                                </td>
                                <td><a href="<?=URL::to('encomienda/'.$encomienda->id)?>" class="label label-success">
                                    Ver</a>
                                </td>
                                <td>
                                    @if(Auth::user()->agencias_id != $encomienda->agencia)
                                        @if($encomienda->estado == 1)
                                            AUN NO SE ENVIA
                                        @elseif($encomienda->estado == 0)
                                            {{Form::open(array('url'=>'buscar/llega'))}}
                                                {{Form::hidden('encomienda', $encomienda->id)}}
                                                {{Form::button('EN CAMINO', array('type'=>'submit', 'class'=>'btn btn-info btn-xs'))}}
                                            {{Form::close()}}
                                        @elseif($encomienda->estado == 2)
                                            {{ Form::open(array('url' => 'encomienda/'.$encomienda->id)) }}
                                                {{ Form::hidden("_method", "DELETE") }}
                                                {{Form::password('contrasenia', array('class'=>'form-control', 'required'=>''))}}<br>
                                                <input type="submit" class="btn btn-danger btn-xs" value="Recoger">
                                            {{ Form::close() }}
                                        @elseif($encomienda->estado == 3)
                                            YA RECOGIO
                                        @endif
                                    @else
                                        @if($encomienda->estado == 1)
                                            AUN NO SE ENVIA
                                        @elseif($encomienda->estado == 0)
                                            EN CAMINO
                                        @elseif($encomienda->estado == 2)
                                            AUN NO RECOGE
                                        @elseif($encomienda->estado == 3)
                                            YA RECOGIO
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Remitente</th>
                                <th>Salida</th>
                                <th>Destinatario</th>
                                <th>Destino</th>
                                <th>Estado</th>
                                <th>Editar</th>
                                <th>Ver</th>
                                <th>Recoger</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop