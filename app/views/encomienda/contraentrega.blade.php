@extends('administrador')

@section('titulo')
Pago
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos del Usuario
        <small>Pago contra entrega</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=URL::to('#')?>"><i class="fa fa-dashboard"></i> inicio</a></li>
        <li><a href="<?=URL::to('#')?>">encomienda</a></li>
        <li class="active">contra entrega</li>
    </ol>
</section>
<section class="content bg-red text-black">
    <div class="row">
        <div class="col-md-9">
            <div class="box box-primary bg-gray">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header">
                                    <h4 class="box-title">Datos de Usuario</h4>
                                </div>
                                @if($estado == 0)
                                {{Form::open(array('url'=>'contraentrega/cliente'))}}
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-error">
                                                    <input class="form-control dniruc" type="text" name="rucdni" 
                                                    required placeholder="RUC / DNI *">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="usuario" 
                                                    placeholder="USUARIO" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="nombre" 
                                                    placeholder="NOMBRE" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="apellidos" 
                                                    placeholder="APELLIDOS" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="direccion" 
                                                    placeholder="DIRECCION" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="pago" 
                                                    placeholder="PAGO" readonly="" value="S/. {{$encomienda->costo}}.00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if($encomienda->id)
                                            <input type="hidden" name="encomienda" value="{{$encomienda->id}}">
                                            <button type="submit" class="btn btn-primary">Siguiente</button>
                                        @endif
                                    </div>
                                {{Form::close()}}
                                @elseif($estado == 1)
                                {{Form::open(array('url'=>'contraentrega/terminar'))}}
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-error">
                                                    <input class="form-control rucdni" type="text" name="rucdni" 
                                                    required placeholder="RUC / DNI *" value="{{$rucdni}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="usuario" 
                                                    placeholder="USUARIO" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="nombre" 
                                                    placeholder="NOMBRE" value="{{$persona->nombre}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="apellidos" 
                                                    placeholder="APELLIDOS" value="{{$persona->apellidos}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="direccion" 
                                                    placeholder="DIRECCION"  value="" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input class="form-control moneda" type="text" name="pago" 
                                                    placeholder="PAGO" value="{{$encomienda->costo}}" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if($encomienda->id)
                                            <input type="hidden" name="encomienda" value="{{$encomienda->id}}">
                                            <button type="submit" class="btn btn-primary">Terminar</button>
                                        @endif
                                    </div>
                                {{Form::close()}}
                                @elseif($estado == 2)
                                {{Form::open(array('url'=>'contraentrega/terminar'))}}
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-error">
                                                    <input class="form-control rucdni" type="text" name="rucdni" 
                                                    required placeholder="RUC / DNI *" value="{{$rucdni}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="usuario" 
                                                    placeholder="USUARIO" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control mayusculas" type="text" name="nombre" 
                                                    placeholder="NOMBRE" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control mayusculas" type="text" name="apellidos" 
                                                    placeholder="APELLIDOS" required>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="direccion" 
                                                    placeholder="DIRECCION"  value="" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input class="form-control moneda" type="text" name="pago" 
                                                    placeholder="PAGO" value="{{$encomienda->costo}}" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if($encomienda->id)
                                            <input type="hidden" name="encomienda" value="{{$encomienda->id}}">
                                            <button type="submit" class="btn btn-primary">Terminar</button>
                                        @endif
                                    </div>
                                {{Form::close()}}
                                @elseif($estado == 3)
                                {{Form::open(array('url'=>'venta'))}}
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-error">
                                                    <input class="form-control rucdni" type="text" name="rucdni" 
                                                    required placeholder="RUC / DNI *" value="{{$dniruc}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="usuario" 
                                                    placeholder="USUARIO" value="{{$empresa->cliente}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="nombre" 
                                                    placeholder="NOMBRE" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="apellidos" 
                                                    placeholder="APELLIDOS" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="direccion" 
                                                    placeholder="DIRECCION" value="{{$empresa->direccion}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input class="form-control moneda" type="text" name="pago" 
                                                    placeholder="PAGO" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if($venta->id)
                                            <input type="hidden" name="venta" value="{{$venta->id}}">
                                            <button type="submit" class="btn btn-primary">Terminar</button>
                                        @endif
                                    </div>
                                {{Form::close()}}
                                @elseif($estado == 4)
                                {{Form::open(array('url'=>'contraentrega/terminar'))}}
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group has-error">
                                                    <input class="form-control rucdni" type="text" name="rucdni" 
                                                    required placeholder="RUC / DNI *" value="{{$rucdni}}" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input class="form-control mayusculas" type="text" name="usuario" 
                                                    placeholder="USUARIO" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="nombre" 
                                                    placeholder="NOMBRE" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="apellidos" 
                                                    placeholder="APELLIDOS" readonly="">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input class="form-control mayusculas" type="text" name="direccion" 
                                                    placeholder="DIRECCION" required>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input class="form-control moneda" type="text" name="pago" 
                                                    placeholder="PAGO" value="{{$encomienda->costo}}" readonly="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        @if($encomienda->id)
                                            <input type="hidden" name="encomienda" value="{{$encomienda->id}}">
                                            <button type="submit" class="btn btn-primary">Terminar</button>
                                        @endif
                                    </div>
                                {{Form::close()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop