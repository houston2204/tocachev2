@extends('administrador')

@section('titulo')
Error
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Auto Repetido
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">error</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="error-page">
        <h2 class="headline">301</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Algo salio mal.</h3>
            <p>
                Al parecer la placa del auto o el auto mismo se está repitiendo en la base de datos, o está 
                registrado con otro conductor. <br>Le recomendamos buscar este auto en la <a href="<?=URL::to('conductor')?>">
                lista de autos</a> y editar al conductor que maneja.
                O mientras tanto, puede <a href="<?=URL::to('administrador')?>">regesar al inicio</a> para seguir trabajando.
            </p>
        </div>
    </div><!-- /.error-page -->

</section><!-- /.content -->
@stop

@section('scripts')

@stop