@extends('administrador')

@section('titulo')
Error
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Conductor Repetido
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Conductor</a></li>
        <li class="active">error</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <div class="error-page">
        <h2 class="headline">201</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Algo salio mal.</h3>
            <p>
                Al parecer el DNI del conductor o el conductor mismo se está repitiendo en la base de datos, o está 
                registrado con otro vehículo. <br>Le recomendamos buscar este coductor en la <a href="<?=URL::to('conductor')?>">
                lista de conductores</a> y editar el auto que maneja.
                O mientras tanto, puede <a href="<?=URL::to('administrador')?>">regesar al inicio</a> para seguir trabajando.
            </p>
        </div>
    </div><!-- /.error-page -->

</section><!-- /.content -->
@stop

@section('scripts')

@stop