@extends('administrador')

@section('titulo')
Terminal
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos de la llegada
        <small>Datos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Servicios</a></li>
        <li class="active">Terminal</li>
    </ol>
</section>

<!-- Main content -->
<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-5">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 200px;">Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>Día</td>
                            <td>{{date('d-m-Y', strtotime($llegada->created_at)-18000)}}</td>
                        </tr>
                        <tr>
                            <td>Hora</td>
                            <td>{{date("h:i:s A", strtotime($llegada->created_at)-18000)}}</td>
                        </tr>
                        <tr>
                            <td>Conductor</td>
                            <td>{{$llegada->conductor}}</td>
                        </tr>
                        <tr>
                            <td>Auto</td>
                            <td>{{$llegada->auto}}</td>
                        </tr>
                        <tr>
                            <td>Destino</td>
                            <td>{{$llegada->destino}}</td>
                        </tr>
                        <tr>
                            <td>Costo</td>
                            <td>S./ {{$llegada->costo}}.00</td>
                        </tr>
                        <tr>
                            <td>Cajero</td>
                            <td>{{$llegada->cajero}}</td>
                        </tr>
                        <tr>
                            <td>Salida</td>
                            <td>
                            @if($llegada->estado)
                            	En Espera
                            @else
                            	{{date("h:i:s A", strtotime($llegada->updated_at)-18000)}}
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="<?=URL::to('llegada')?>" class="btn btn-success">ok</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop