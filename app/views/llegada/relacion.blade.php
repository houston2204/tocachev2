<!DOCTYPE html>
<html>
<head>
	<title>Relación</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body style="width:795px">
	<div>
		<h1>Salida del Terminal</h1>
	</div>
	<table class="table" style="width:260px">
		<tr>
			<th style="width:130px">Auto</th>
			<td>{{$llegada->auto}}</td>
		</tr>
		<tr>
			<th>Fecha de Salida</th>
			<td>{{date("d-m-Y", strtotime($llegada->updated_at)-18000)}}</td>
		</tr>
		<tr>
			<th>Hora de salida</th>
			<td>{{date("h:i:s A", strtotime($llegada->updated_at)-18000)}}</td>
		</tr>
		<tr>
			<th>Agencia de Salida</th>
			<td>{{$llegada->salida}}</td>
		</tr>
		<tr>
			<th>Destino</th>
			<td>{{$llegada->destino}}</td>
		</tr>
	</table>
	<h2>Realción de Pasajeros</h2>
	<table class="table" border="0"  style="width:350px">
		<tr>
			<th style="width:250px">Pasajero</th>
			<th>DNI</th>
		</tr>
		@foreach($pasajero as $i)
			@if(Contrato::find($i)->servicios_id == 2)
				<tr>
					<td>
						{{Persona::find(Contrato::find($i)->cliente)->nombre}}
						{{Persona::find(Contrato::find($i)->cliente)->apellidos}}
					</td>
					<td>{{Persona::find(Contrato::find($i)->cliente)->dni}}</td>
				</tr>
			@endif
		@endforeach
	</table>
<h2>Realción de Encomiendas</h2>
<table class="table">
	<tr>
		<th>Remitente</th>
		<th>Salida</th>
		<th>Destinatario</th>
		<th>DNI</th>
		<th>Destino</th>
	</tr>
	@foreach($pasajero as $i)
		@if(Contrato::find($i)->servicios_id == 3)
			<tr>
				<td>
					{{Persona::find(Contrato::find($i)->cliente)->nombre}}
					{{Persona::find(Contrato::find($i)->cliente)->apellidos}}
				</td>
				<td>{{Contrato::find($i)->salida}}</td>
				<td>
					{{Persona::find(Contrato::find($i)->receptor)->nombre}}
					{{Persona::find(Contrato::find($i)->receptor)->apellidos}}
				</td>
				<td>{{Persona::find(Contrato::find($i)->receptor)->dni}}</td>
				<td>{{Contrato::find($i)->destino}}</td>
			</tr>
		@endif
	@endforeach
</table>
<div  style="width:300px">
	<br>
	<a href="<?=URL::to('llegada')?>" class="btn btn-success btn-xs no-print">Terminar</a>
	<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
</div>
</body>
</html>