@extends('administrador')

@section('titulo')
Nueva
Llegada
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Ingresar
        Datos
        <small>
        Nueva
        llegada
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">llegada</a></li>
        <li class="active">
            Nuevo
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                {{Form::open(array('url'=>'llegada'))}}
                    <div class="box-body">
                        <label>Llegada</label>
                        <div class="form-group has-error">
                            <input type="text" name="numero" class="form-control" placeholder="Licencia *"
                            value="{{$licencia->numero}}" readonly="">
                            <input type="hidden" name="licencia" value="{{$licencia->id}}">
                        </div>
	                    <div class="form-group">
	                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" 
	                        value="{{$persona->nombre}}" readonly="">
                            <input type="hidden" name="persona" value="{{$persona->id}}">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" 
	                        value="{{$persona->apellidos}}" readonly="">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="placa" class="form-control" placeholder="Placa" 
	                        value="{{$auto->placa}}" readonly="">
                            <input type="hidden" name="auto" value="{{$auto->id}}">
	                    </div>
                        <div class="form-group">
                            <input type="text" name="destino" class="form-control mayusculas" placeholder="Destino" 
                            value="">
                        </div>
                        <div class="form-group has-error">
                            <input type="text" name="costo" class="form-control moneda" placeholder="Costo *" 
                            value="" required>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <a href="<?=URL::to('llegada')?>" class="btn bg-maroon">Cancelar</a>
                        </div>
	                </div>
                {{Form::close()}}
	            </div>
	        </div>  
    </div>
</section>
@stop

@section('scripts')

@stop