@extends('trabajador')

@section('titulo')
Llegadas
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
<meta http-equiv="refresh" content="30">
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Llegadas
        <small>lista completa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Llegadas</a></li>
        <li class="active">llegada de Autos y Conductores</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-10">

            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Llegada</th>
                                <th>Conductor</th>
                                <th>Auto</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contratos as $contrato)
                            @if(strtotime($contrato->created_at)-18000 > $hoy)
                            <tr>
                                <td>{{date("h:i:s A", strtotime($contrato->created_at)-18000)}}</td>
                                <td>{{$contrato->conductor}}</td>
                                <td>{{$contrato->auto}}</td>
                                <td>{{$contrato->destino}}</td>
                                <td>
                                    {{ Form::open(array('url' => 'llegada/'.$contrato->id)) }}
                                    {{ Form::hidden("_method", "DELETE") }}
                                    @if($contrato->estado)
                                    <a href="<?=URL::to('llegada/'.$contrato->id.'/edit')?>" class="label label-warning">esperando</a>
                                    @else
                                    {{date("h:i:s A", strtotime($contrato->updated_at)-18000)}}
                                    @endif
                                    {{ Form::close() }}
                                </td>
                                <td><a href="<?=URL::to('llegada/'.$contrato->id)?>" class="label label-success">Ver</a></td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Llegada</th>
                                <th>Conductor</th>
                                <th>Auto</th>
                                <th>Destino</th>
                                <th>Salida</th>
                                <th>Ver</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop