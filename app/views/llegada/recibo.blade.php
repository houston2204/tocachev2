<!DOCTYPE html>
<html>
<head>
	<title>Recibo Llegada</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body  style="width:216px">
	<h3>Recibo de Llegada</h3>
	<div>
		<table class="table" border="0">
			<tr>
				<td colspan="2" style="text-align:center;">Tocache express</td>
			</tr>
			<tr style="text-align:center;">
				<td width="108px">Día</td>
				<td width="108px">Hora</td>
			</tr>
			<tr style="text-align:center;">
				<td>{{date('d-m-Y', strtotime($llegada->created_at)-18000)}}</td>
				<td>{{date("h:i:s A", strtotime($llegada->created_at)-18000)}}</td>
			</tr>
		</table>
		<table class="table" border="0">
			<tr>
				<td>Conductor</td>
				<td>{{$llegada->conductor}}</td>
			</tr>
			<tr>
				<td>Auto</td>
				<td>{{$llegada->auto}}</td>
			</tr>
			<tr>
				<td>Destino</td>
				<td>{{$llegada->destino}}</td>
			</tr>
			<tr>
				<td>Costo</td>
				<td>S./ {{$llegada->costo}}</td>
			</tr>
			<tr>
				<td>Son</td>
				<td>{{$letras}}</td>
			</tr>
			<tr>
				<td>Cajero</td>
				<td>{{$llegada->cajero}}</td>
			</tr>
		</table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('llegada')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>