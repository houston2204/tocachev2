@extends('administrador')

@section('titulo')
Nueva Llegada
@stop

@section('estilos')
<link rel="stylesheet" type="text/css" href="<?=URL::to('css/jquery-ui/jquery-ui.css')?>">
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Ingresar Datos
        <small>
        Nueva llegada
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">llegada</a></li>
        <li class="active">
            Nuevo
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
            <div class="col-md-6">
                @if(Session::has('rojo'))
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Alerta!</b> {{ Session::get('rojo')}}
                    </div>
                @elseif(Session::has('verde'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Exelente!</b> {{ Session::get('verde')}}
                    </div>
                @elseif(Session::has('naranja'))
                    <div class="alert alert-warning alert-dismissable">
                        <i class="fa fa-info"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Cuidado!</b> {{ Session::get('naranja')}}
                    </div>
                @endif
                <div class="box box-success">
                    <div class="box-body">
                        <label>Llegada</label>
                        {{Form::open(array('url'=>'buscar/buscar-conductor'))}}
                        <div class="form-group has-error">
                            <input type="text" name="placa" class="form-control placas mayusculas" placeholder="PLACA *"
                            value="{{$licencia->numero}}" autofocus>
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                        {{Form::close()}}
	                    <div class="form-group">
	                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" 
	                        value="{{$persona->nombre}}" readonly="">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" 
	                        value="{{$persona->apellidos}}" readonly="">
	                    </div>
	                    <div class="form-group">
	                        <input type="text" name="placa" class="form-control" placeholder="Placa" 
	                        value="{{$auto->placa}}" readonly="">
	                    </div>
	                     <div class="form-group has-error">
	                        <input type="text" name="costo" class="form-control" placeholder="Costo *" 
	                        value="" readonly="">
	                    </div>
	                </div>
	            </div>
	        </div>        
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/autocomplete/jquery-ui.min.js')?>" type="text/javascript"></script>
<script>
  $(function(){
    var autocompletar = new Array();
    @foreach($autos as $l)
       autocompletar.push('{{$l->placa}}');
    @endforeach
     $(".placas").autocomplete({ //Usamos el ID de la caja de texto donde lo queremos
       source: autocompletar //Le decimos que nuestra fuente es el arreglo
     });
  });
</script>
@stop