@extends('administrador')

@section('titulo')
Panel de Control
@stop

@section('estilos')

@stop

@section('contenido')
<!-- Encabezado de la página -->
<section class="content-header">
    <h1>
        Inicio
        <small>Panel de control</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Panel de control</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <h4 class="page-header">
        Panel de Control
        <small>Bienvenido al Panel de inicio de Tocache express. Le deseamos un  buen día de trabajo.</small>
    </h4>
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Llegadas
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Pasajes
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>
                        #
                    </h3>
                    <p>
                        Encomiendas
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios7-pricetag-outline"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        %
                    </h3>
                    <p>
                        Telegiros
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">
                    Ingresar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->

</section><!-- /.content -->
@stop

@section('scripts')

@stop