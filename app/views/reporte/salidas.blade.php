@extends('administrador')

@section('titulo')
Reporte
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Reportes de Salidas
        <small>Salidas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Reportes</a></li>
        <li class="active">Salidas</li>
    </ol>
</section>

<div class="content bg-green text-black">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
                <div class="box-header">
                    <h3 class="box-title">Reporte de Salidas de las Agencias</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Agencia</th>
                            <th>Fecha</th>
                            <th>Hora Llegada</th>
                            <th>Conductor</th>
                            <th>Auto</th>
                            <th>Destino</th>
                            <th>Hora Salida</th>
                            <th>Cajero</th>
                            <th>Abono</th>
                        </tr>
                        @foreach($servicios as $servicio)
                        <tr>
                        	<td>{{$servicio->salida}} </td>
                        	<td>{{date('d-m-Y', strtotime($servicio->created_at)-18000)}} </td>
                        	<td>{{date('h:i:s A', strtotime($servicio->created_at)-18000)}}</td>
                        	<td>{{$servicio->conductor}} </td>
                        	<td>{{$servicio->auto}}</td>
                        	<td>{{$servicio->destino}}</td>
                        	<td>{{date('h:i:s A', strtotime($servicio->updated_at)-18000)}}</td>
                        	<td>{{$servicio->cajero}}</td>
                        	<td>S/. {{$servicio->costo}}.00</td>
                        </tr>
                        @endforeach
                        <tr>
                        	<td colspan="7"></td>
                        	<th>Total</th>
                        	<th>S/. {{$total}}.00</th>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
		</div>
	</div>
</div>
@stop

@section('scripts')

@stop