@extends('administrador')

@section('titulo')
Reportes
@stop

@section('estilos')
<link href="<?=URL::to('css/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=URL::to('css/iCheck/all.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Reportes de Movimientos
        <small>Buscar</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Reportes</a></li>
        <li class="active">Buscar</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
        	@if(Session::has('rojo'))
	            <div class="alert alert-danger alert-dismissable">
	                <i class="fa fa-ban"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <b>Alerta!</b> {{ Session::get('rojo')}}
	            </div>
	        @elseif(Session::has('verde'))
	            <div class="alert alert-success alert-dismissable">
	                <i class="fa fa-check"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <b>Exelente!</b> {{ Session::get('verde')}}
	            </div>
	        @elseif(Session::has('naranja'))
	            <div class="alert alert-warning alert-dismissable">
	                <i class="fa fa-warning"></i>
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <b>Cuidado!</b> {{ Session::get('naranja')}}
	            </div>
	        @endif
        	{{Form::open(array('url'=>'reporte/generar'))}}
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Rango de Tiempo y Reporte</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label>
                            <input type="radio" name="servicio" value="1" class="flat-red" checked/> Salidas<br>
                        </label><br>
                        <label>
                            <input type="radio" name="servicio" value="2" class="flat-red"/> Pasajes<br>
                        </label><br>
                        <label>
                            <input type="radio" name="servicio" value="3" class="flat-red"/> Encomiendas<br>
                        </label><br>
                        <label>
                            <input type="radio" name="servicio" value="4" class="flat-red"/> Telegiros<br>
                        </label><br>
                        <label>
                            <input type="radio" name="servicio" value="5" class="flat-red"/> Total<br>
                        </label><br>
                    </div>
                	<div class="form-group">
                        <label>Rango de Días:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="rango" class="form-control pull-right" id="reservation" readonly="" 
                            required/>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <a href="<?=URL::to('administrador')?>" class="btn btn-warning">Cancelar</a>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>

</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/daterangepicker/daterangepicker.js')?>" type="text/javascript"></script>
<script type="text/javascript">

    $(function() {
        
        $('#reservation').daterangepicker();
                        
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });
    });
</script>
@stop