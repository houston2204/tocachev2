@extends('administrador')

@section('titulo')
Acerca de
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        ACERCA DE
        <small>SIGCTE</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Acerda de</a></li>
        <li class="active">SIGCTE</li>
    </ol>
</section>

<div class="pad margin no-print">
    <div class="alert alert-info" style="margin-bottom: 0!important;">
        <i class="fa fa-info"></i>
        <b>Nota:</b> Esta página contiene información general del sistema y de su desarrollador. Para cualquier consulta
        comunicarse con el desarrollador.
    </div>
</div>

<section class="content invoice">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Sistema Integrado de Gestión y Comercio - Tocache Express (SIGCTE)
                <small class="pull-right">Fecha {{date('d/m/Y')}}</small>
            </h2>
        </div>
    </div>
    <div class="row invoice-info">
    	<div class="col-sm-12 invoice-col">
    		<p style="text-align: justify">
	    		<strong>El Sistema Integrado de Gestión y Comercio - Tocache Express (SIGCTE)</strong> está diseñado 
	    		para la gestión de la información de Tocache Express, teniendo así todos los datos correspondiente a los 
	    		conductores, los autos y sus respectivos dueños.<br>
	    		Administrando esa información de manera que sea más accesible a los usuarios y administradores de las 
	    		agencias, emitiendo reportes de los conductores y los autos, alertando sobre documentos vencidos o a
	    		punto de vencer, ademas de calcular el movimiento diario total y por servicios en cada momento.<br>
	    		El <strong>SIGCTE</strong> tiene como función principal ayudar y facilitar la gestion de documentos 
	    		y movimientos diarios a los usuarios (cajeros) de cada agencia, así emitir un boleto, ticket o factura será 
	    		más sencillo y rápido con el <strong>SIGCTE</strong>.<br>
	    		Por último, podemos decir que el manejo de la caja será un proceso más automatizado con este sistema. 
	    		Pudiendo realizar emision de pasajes, encomiendas y telegiros. Además de hacer el seguimiento de la llegada
	    		y/o recojo de las encomiendas y telegiros.
    		</p>
    	</div>
    </div>
    <hr>
    <div class="row invoice-info">
	    <div class="col-sm-12">
    		<h2 class="page-header">
                <i class="fa fa-globe"></i> Desarrollador del Sistema.
            </h2>
	    	<div class="col-sm-6">
	    		<img src="<?=URL::to('img/siprom.jpg')?>">
	    	</div>
	    	<div class="col-sm-6">
		    	<div style="text-align: justify">
		    		El <strong>SIGCTE</strong> fue diseñado y desarrollado por <strong>&#174;SIPROM</strong> bajo la tutoría de
		    		<strong>Tocache Express</strong> para su uso en el área de transporte de pasajeros y encomiendas, y el 
		    		servicio de telegiros.<br>
		    		La principal función de este sistema es la de automatizar los procesos de esta empresa, facilitando y 
		    		agilizando el trabajo diario en las agencias.
		    	</div>
	    	</div>
	        <div class="col-sm-4 invoice-col">
	            <address>
	                <strong>Contacto</strong><br>
	                Jr. Monseñor Sardinas 118 Huánuco<br>
	                (Frente a la UNHEVAL)<br>
	                Teléfonos: 962810000, 952581344<br/>
	                Email: ventas@sipromsac.com<br/>
	                Síguenos en <a href="https://www.facebook.com/siprom2012" target="_blank">
	                				<i class="fa fa-facebook"></i>acebook
	                			</a>
	            </address>
	        </div>
	    </div>
    </div>

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
        </div>
    </div>
</section><!-- /.content -->
@stop

@section('scripts')

@stop