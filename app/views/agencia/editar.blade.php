@extends('administrador')

@section('titulo')
Editar Agencia
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Modificar Datos
        <small>Editar</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Agencia</a></li>
        <li class="active">Editar</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                {{Form::open(array('url'=>'agencia/'.$agencia->id))}}
                    <div class="box-body">
                        <label>Datos de la Agencia</label>
                        <div class="form-group has-error">
                            <input type="text" name="nombre" class="form-control mayusculas" placeholder="Nombre *"
                            value="{{$agencia->nombre}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="telefono" class="form-control mayusculas telefono" placeholder="Teléfono"
                            value="{{$agencia->telefono}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="lugar" class="form-control mayusculas" 
                            placeholder="Dirección Av. Jr. Urb. AAHH. Res. ..."
                            value="{{Direccion::find($agencia->direcciones_id)->lugar}}">
                        </div>
                        <div class="form-group">
                            <input type="text" name="distrito" class="form-control mayusculas" placeholder="Distrito"
                            value="{{Direccion::find($agencia->direcciones_id)->distrito}}">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="provincia" class="form-control muyusculas" placeholder="Provincia"
                            value="{{Direccion::find($agencia->direcciones_id)->provincia}}">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="departamento" class="form-control mayusculas" placeholder="Departamento"
                            value="{{Direccion::find($agencia->direcciones_id)->departamento}}">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-black">Guardar</button>
                        <a href="<?=URL('agencia')?>" class="btn bg-maroon">Cancelar</a>
                    </div>
                    {{Form::hidden('_method', 'put')}}
                {{Form::close()}}
            </div>

        </div>
    </div>
</section>
@stop

@section('scripts')

@stop