@extends('administrador')

@section('titulo')
Agencia {{$agencia->nombre}}
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos de la Agencia
        <small>Datos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Agencia</a></li>
        <li class="active">Datos</li>
    </ol>
</section>

<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-6">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{{$agencia->nombre}}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{{$agencia->telefono}}</td>
                        </tr>
                        <tr>
                            <td>Dirección</td>
                            <td>{{Direccion::find($agencia->direcciones_id)->lugar}}  
                            	{{Direccion::find($agencia->direcciones_id)->distrito}}  
                            	{{Direccion::find($agencia->direcciones_id)->provincia}}  
                            	{{Direccion::find($agencia->direcciones_id)->departamento}}
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <a href="<?=URL::to('agencia')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('agencia/'.$agencia->id.'/edit')?>" class="btn btn-info">Editar</a>
        </div>
    </div>
</section>
@stop

@section('')

@stop