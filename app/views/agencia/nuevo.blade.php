@extends('administrador')

@section('titulo')
Nueva Agencia
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Ingresar Datos
        <small>Nuevo</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Agencia</a></li>
        <li class="active">Nuevo</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-success">
                {{Form::open(array('url'=>'agencia'))}}
                    <div class="box-body">
                        <label>Datos de la Agencia</label>
                        <div class="form-group has-error">
                            <input type="text" name="nombre" class="form-control mayusculas" placeholder="Agencia *" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="telefono" class="form-control telefono" placeholder="TELEFONO (9 DIGITOS)">
                        </div>
                        <div class="form-group">
                            <input type="text" name="lugar" class="form-control mayusculas" 
                            placeholder="Dirección Av. Jr. Urb. AAHH. Res. ...">
                        </div>
                        <div class="form-group">
                            <input type="text" name="distrito" class="form-control mayusculas" placeholder="Distrito">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="provincia" class="form-control mayusculas" placeholder="Provincia">
                        </div>
                        <div class="form-group">
                        	<input type="text" name="departamento" class="form-control mayusculas" placeholder="Departamento">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary bg-black">Guardar</button>
                        <a href="<?=URL('agencia')?>" class="btn bg-maroon">Cancelar</a>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')
@stop