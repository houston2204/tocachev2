@extends('administrador')

@section('titulo')
Agencias
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Lista de Agencias
        <small>Todas las Agencias</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Agencia</a></li>
        <li class="active">Lista de agencias</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-8">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Agencia</th>
                                <th>Telefono</th>
                                <th>Dirección</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Borrar</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($agencias as $agencia)
                            <tr>
                                <td>{{$agencia->nombre}}</td>
                                <td>{{$agencia->telefono}}</td>
                                <td>{{Direccion::find($agencia->direcciones_id)->lugar}} 
                                	{{Direccion::find($agencia->direcciones_id)->distrito}} 
                                	{{Direccion::find($agencia->direcciones_id)->provincia}} 
                                	{{Direccion::find($agencia->direcciones_id)->departamento}}</td>
                                <td><a href="<?=URL::to('agencia/'.$agencia->id)?>" class="label label-success">Ver</a></td>
                                <td><a href="<?=URL::to('agencia/'.$agencia->id.'/edit')?>" class="label label-info">Editar</a></td>
                                <td>
                                	{{ Form::open(array('url' => 'agencia/'.$agencia->id)) }}
                                    {{ Form::hidden("_method", "DELETE") }}
                                    	<input type="submit" class="btn btn-danger btn-xs" value="Borrar">
                                    {{ Form::close() }}</td>
                            	</tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Agencia</th>
                                <th>Telefono</th>
                                <th>Dirección</th>
                                <th>Ver</th>
                                <th>Editar</th>
                                <th>Borrar</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop