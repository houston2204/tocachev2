@extends('administrador')

@section('titulo')
Tele Giros 
@stop

@section('estilos')
<link href="<?=URL::to('css/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Telegiros
        <small>Todas los Telegiros</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Telegiros</a></li>
        <li class="active">Todos</li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
        <div class="col-md-10">
            @if(Session::has('rojo'))
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alerta!</b> {{ Session::get('rojo')}}
                </div>
            @elseif(Session::has('verde'))
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Exelente!</b> {{ Session::get('verde')}}
                </div>
            @elseif(Session::has('naranja'))
                <div class="alert alert-warning alert-dismissable">
                    <i class="fa fa-info"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Cuidado!</b> {{ Session::get('naranja')}}
                </div>
            @endif
            <div class="box">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Remitente</th>
                                <th>Salida</th>
                                <th>Destinatario</th>
                                <th>Destino</th>
                                <th>Monto</th>
                                <th>Recoger</th>
                                <th>Editar</th>
                                <th>Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($giros as $giro)
                            <tr>
                                <td>
                                    {{Persona::find($giro->cliente)->nombre}} 
                                    {{Persona::find($giro->cliente)->apellidos}}
                                </td>
                                <td>{{$giro->salida}}</td>
                                <td>
                                    {{Persona::find($giro->receptor)->nombre}} 
                                    {{Persona::find($giro->receptor)->apellidos}}
                                </td>
                                <td>{{$giro->destino}}</td>
                                <td>S/. {{$giro->descripcion}}</td>
                                <td>
                                    @if($giro->agencia == Auth::user()->agencias_id)
                                        @if($giro->estado == 2)
                                            {{date("h:i:s A", strtotime($giro->updated_at)-18000)}}
                                        @else
                                            Sin Recoger
                                        @endif
                                    @else
                                        @if($giro->estado == 0)
                                            {{ Form::open(array('url' => 'giro/'.$giro->id)) }}
                                            {{ Form::hidden("_method", "DELETE") }}
                                                <input type="submit" class="btn btn-danger btn-xs" value="Recoger">
                                            {{ Form::close() }}
                                        @else
                                            {{date("h:i:s A", strtotime($giro->updated_at)-18000)}}
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if($giro->agencia == Auth::user()->agencias_id)
                                    @if($giro->estado == 0)
                                        <a href="<?=URL::to('giro/'.$giro->id.'/edit')?>" class="label label-info">Editar</a>
                                    @else
                                        Cobrado
                                    @endif
                                @else
                                    @if($giro->estado == 0)
                                        No Autorizado
                                    @else
                                        Cobrado
                                    @endif
                                @endif
                                </td>
                                <td><a href="<?=URL::to('giro/'.$giro->id)?>" class="label label-success">Ver</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Remitente</th>
                                <th>Salida</th>
                                <th>Destinatario</th>
                                <th>Destino</th>
                                <th>Monto</th>
                                <th>Recoger</th>
                                <th>Editar</th>
                                <th>Ver</th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/datatables/jquery.dataTables.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/datatables/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop