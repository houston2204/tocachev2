<!DOCTYPE html>
<html>
<head>
	<title>Telegiro</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body  style="width:454px">
	<div>
		<h1>Tocache Express</h1>
		<h2>Recibí Conforme</h2>
	</div>
	<div>
		<table class="table" border="0">
			<tr>
				<td>DESTINATARIO:</td>
				<td colspan="3">{{Persona::find($giro->receptor)->nombre}}
				{{Persona::find($giro->receptor)->apellidos}}</td>
			</tr>
			<tr>
				<td>Doc. Ident.:</td>
				<td>{{Persona::find($giro->receptor)->dni}}</td>
				<td>Agencia:</td>
				<td>{{Agencia::find(Auth::user()->agencias_id)->nombre}}</td>
			</tr>
			<tr>
				<td>Cantidad Recibida: </td>
				<td>S/. {{$giro->descripcion}}</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Son:</td>
				<td> 00/100 Nuevos Soles</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Tocache Express</td>
				<td>{{Persona::find(Auth::user()->personas_id)->nombre}}
					{{Persona::find(Auth::user()->personas_id)->apellidos}}</td>
				<td>Fecha de Emisión</td>
				<td>{{date('d-m-Y', strtotime($giro->created_at)-18000)}}</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td style="text-align: centex">FIRMA</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('giro')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>