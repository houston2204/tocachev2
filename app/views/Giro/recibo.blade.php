<!DOCTYPE html>
<html>
<head>
	<title>Pasaje</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=URL::to('css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
</head>
<body  style="width:454px">
	<div>
		<h1>Tocache Express</h1>
	</div>
	<div>
		<table class="table" border="0">
			<tr>
				<td>USUARIO:</td>
				<td width="300px">{{$encomienda->usuario}}</td>
				<td>R.U.C.:</td>
				<td width="104px">{{$encomienda->ruc}}</td>
			</tr>
			<tr>
				<td>REMITENTE:</td>
				<td colspan="3">{{Persona::find($encomienda->cliente)->nombre}}
				{{Persona::find($encomienda->cliente)->apellidos}}</td>
			</tr>
			<tr>
				<td>Doc. Ident.:</td>
				<td>{{Persona::find($encomienda->cliente)->dni}}</td>
				<td>SALIDA:</td>
				<td>{{$encomienda->salida}}</td>
			</tr>
			<tr>
				<td>DESTINATARIO:</td>
				<td colspan="3">{{Persona::find($encomienda->receptor)->nombre}}
				{{Persona::find($encomienda->receptor)->apellidos}}</td>
			</tr>
			<tr>
				<td>Doc. Ident.:</td>
				<td>{{Persona::find($encomienda->receptor)->dni}}</td>
				<td>DESTINO:</td>
				<td>{{$encomienda->destino}}</td>
			</tr>
			<tr>
				<td>Cantidad Enviada: </td>
				<td>S/. {{$encomienda->descripcion}}</td>
				<td>Valor Venta:</td>
				<td>S/. {{$encomienda->costo}}</td>
			</tr>
			<tr>
				<td>Son:</td>
				<td>{{$letras}}</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>Tocache Express</td>
				<td>{{$encomienda->cajero}}</td>
				<td>Fecha de Emisión</td>
				<td>{{date('d-m-Y', strtotime($encomienda->created_at)-18000)}}</td>
			</tr>
		</table>
	</div>
	<div>
		<br>
		<a href="<?=URL::to('administrador')?>" class="btn btn-success btn-xs no-print">Terminar</a>
		<input type="button" name="imprimir" value="Imprimir" onclick="window.print();" class="btn btn-warning btn-xs pull-right no-print">
	</div>
</body>
</html>