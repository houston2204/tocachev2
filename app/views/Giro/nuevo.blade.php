@extends('administrador')

@section('titulo')
Enviar Telegiro
@stop

@section('estilos')
<link href="<?=URL::to('css/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=URL::to('css/iCheck/all.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=URL::to('css/colorpicker/bootstrap-colorpicker.min.css')?>" rel="stylesheet"/>
<link href="<?=URL::to('css/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet"/>
@stop

@section('contenido')
<section class="content-header">
    <h1>
        Ingresar Datos
        <small>
        Telegiro
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Telegiro</a></li>
        <li class="active">
            Enviar
        </li>
    </ol>
</section>

<section class="content bg-green text-black">
    <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-body">
                        
                        <label>Remitente</label>
                        {{Form::open(array('url'=>'giro/'.$encomienda->id))}}
                        <div class="row">
                        	<div class="col-md-8">
                        		<div class="form-group">
                        			<input type="text" name="usuario" class="form-control mayusculas" placeholder="Razón Social" 
                                    readonly="" value="{{$usuario}}">
                        		</div>
                        	</div>
                        	<div class="col-md-4">
                        		<div class="form-group">
                        			<input type="text" name="ruc" class="form-control ruc" placeholder="RUC" readonly=""
                        			value="{{$ruc}}">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="nombreremitente" class="form-control mayusculas" placeholder="Nombre" 
                                    readonly="" value="{{$nombreremitente}}">
                        		</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="apellidosremitente" class="form-control mayusculas" placeholder="Apellidos" 
                                    readonly="" value="{{$apellidosremitente}}">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="dniremitente" class="form-control dni" placeholder="DNI *" 
                                    readonly="" value="{{$dniremitente}}">
                        		</div>
                        	</div>
                        </div>
                        <label>Destinatario</label>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="nombredestinatario" class="form-control mayusculas" placeholder="Nombre" 
                        			required value="{{$destinatario->nombre}}">
                        		</div>
                        	</div>
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="apellidosdestinatario" class="form-control mayusculas" 
                                    placeholder="Apellidos" required value="{{$destinatario->apellidos}}">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<div class="form-group has-error">
                        			<input type="text" name="dnidestinatario" class="form-control dni" placeholder="DNI *" 
                                    readonly="" value="{{$dnidestinatario}}">
                        		</div>
                        	</div>
                        </div>
                        <div class="row">
                        	<div class="col-md-12">
                        		<div class="form-group has-error">
                        			<input type="text" name="destino" class="form-control mayusculas" placeholder="Destino" required
                                    value="{{$encomienda->destino}}">
                        		</div>
                        	</div>
                            
                        </div>
                        @if($encomienda->id)
                        <div class="form-group has-error">
                            <input type="text" name="descripcion" class="form-control" placeholder="Cantidad a enviar..."
                            value="S/. {{$encomienda->descripcion}}">
                        </div>
                        @else
                        <div class="form-group has-error">
                            <input type="text" name="descripcion" class="form-control moneda" placeholder="Cantidad a enviar..."
                            value="">
                        </div>
                        @endif
                        <div class="row">
                        	<div class="col-md-6 right">
                        		<div class="form-group has-error input-group">
                        			<input type="text" name="costo" class="form-control moneda" placeholder="Costo" required
                                    value="{{$encomienda->costo}}">
                        		</div>
                        	</div>
                        </div>
                        <div class="form-group has-error">
                            <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                        @if($encomienda->id)
                        {{Form::hidden('_method', 'put')}}
                        @endif
                        {{Form::close()}}
	                </div>
	            </div>
	        </div>        
    </div>
</section>
@stop

@section('scripts')
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.date.extensions.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/input-mask/jquery.inputmask.extensions.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/daterangepicker/daterangepicker.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/colorpicker/bootstrap-colorpicker.min.js')?>" type="text/javascript"></script>
<script src="<?=URL::to('js/plugins/timepicker/bootstrap-timepicker.min.js')?>" type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).
                        endOf('month')]
                    },
                    startDate: moment().subtract('days', 29),
                    endDate: moment()
                },
        function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>
@stop