@extends('administrador')

@section('titulo')
Telegiro 
@stop

@section('estilos')

@stop

@section('contenido')
<section class="content-header">
    <h1>
        Datos de la Telegiro
        <small>Datos</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Servicios</a></li>
        <li class="active">Telegiro</li>
    </ol>
</section>

<section class="content bg-green text-black">
	<div class="row">
        <div class="col-md-5">
            <div class="box box-success"> 	
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th style="width: 200px;">Atributos</th>
                            <th>Datos</th>
                        </tr>
                        <tr>
                            <td>USUARIO</td>
                            <td>{{$encomienda->usuario}}</td>
                        </tr>
                        <tr>
                            <td>RUC</td>
                            <td>{{$encomienda->ruc}}</td>
                        </tr>
                        <tr>
                            <td>REMITENTE</td>
                            <td>{{Persona::find($encomienda->cliente)->nombre}}
                            	{{Persona::find($encomienda->cliente)->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td>{{Persona::find($encomienda->cliente)->dni}}</td>
                        </tr>
                        <tr>
                            <td>DESTINATARIO</td>
                            <td>{{Persona::find($encomienda->receptor)->nombre}}
                            	{{Persona::find($encomienda->receptor)->apellidos}}</td>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td>{{Persona::find($encomienda->receptor)->dni}}</td>
                        </tr>
                        <tr>
                            <td>Salida</td>
                            <td>{{$encomienda->salida}}</td>
                        </tr>
                        <tr>
                            <td>Destino</td>
                            <td>{{$encomienda->destino}}</td>
                        </tr>
                        <tr>
                            <td>Cantidad Enviada</td>
                            <td>S/. {{$encomienda->descripcion}}</td>
                        </tr>
                        <tr>
                            <td>Costo</td>
                            <td>S/. {{$encomienda->costo}}.00</td>
                        </tr>
                        <tr>
                            <td>Cajero</td>
                            <td>{{$encomienda->cajero}}</td>
                        </tr>
                        <tr>
                            <td>Recogido</td>
                            <td>
                            @if($encomienda->estado)
                            	En Espera
                            @else
                            	{{date("h:i:s A", strtotime($encomienda->updated_at)-18000)}}
                            @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <a href="<?=URL::to('giro')?>" class="btn btn-success">ok</a>
            <a href="<?=URL::to('recibo')?>" class="btn btn-warning">Recibo</a>
        </div>
    </div>
</section>
@stop

@section('scripts')

@stop