<?php
/*Este es un nuevo comentario*/
/*Condenado sube este*/
Route::get('/', function(){
	if(Auth::user()){
		return Redirect::to('administrador');
	}
	return View::make('login');
});

//Ruta para que un usuario ingrese al sistema.
Route::post('entrar', function(){

	$email = Input::get('dni');
	$password = Input::get('password');
	
	if(Auth::attempt(array('email'=>$email, 'password'=>$password), true)){

		if(Auth::user()->tipo == 1){

			return Redirect::intended('administrador');

		}else{

			return Redirect::intended('trabajador');

		}
	}else{

		return Redirect::to('/');
	}
});


//ruta para ingeras al panel de control del administrador
Route::get('administrador', function()
{
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	return View::make('administrador.inicio')->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

//Ruta para ingresar al panel de control del usuario
Route::get('trabajador', function()
{
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	return View::make('trabajador.inicio')->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

//Ruta para desloguearse y salir del sistema
Route::get('salir', function(){

	Auth::logout();
	return Redirect::to('/');
});

Route::controller('buscar', 'BuscarController');

Route::controller('contraentrega', 'ContraentregaController');

Route::controller('contrasenia', 'ContraseniaController');

//Ruta para administrar las agencias
Route::resource('agencia', 'AgenciaController');

//Ruta para administrar a los usuarios
Route::resource('usuario', 'UsuarioController');

//Ruta para administrar a los conductores
Route::resource('conductor', 'ConductorController');

//Ruta para administrar a los conductores
Route::resource('auto', 'AutoController');

//Ruta para registro de llegadas
Route::resource('llegada', 'LlegadaController');

//Ruta para registro de pasajes
Route::resource('pasaje', 'PasajeController');

//Ruta para registro de pasajes
Route::resource('encomienda', 'EncomiendaController');

//Ruta para registro de giros
Route::resource('giro', 'GiroController');

//Ruta para registro de giros
Route::resource('caja', 'CajaController');

//Ruta para registro de giros
Route::resource('cierre', 'CierreController');

Route::get('sigcte', function(){

	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;

	return View::make('sigcte')->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

Route::controller('reporte', 'ReporteController');

Route::get('licencias', function(){
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	$hoy = date('Y-m-d', strtotime('-5 Hours'));
	$licencias = Licencia::where('vencimiento', '<=', $hoy )->get();
	return View::make('licencias.vencidas')->with('licencias', $licencias)->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

Route::get('seguros', function(){
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	$hoy = date('Y-m-d', strtotime('-5 Hours'));
	$seguros = Seguro::where('vencimiento', '<=', $hoy )->get();
	return View::make('seguros.vencidos')->with('seguros', $seguros)->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

Route::get('licencias/vencer', function(){
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	$hoy = date('Y-m-d', strtotime('-5 Hours'));
	$licencias = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	return View::make('licencias.porvencer')->with('licencias', $licencias)->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});

Route::get('seguros/vencer', function(){
	$lv = 0;
	$lw = 0;
	$sv = 0;
	$sw = 0;
	$hoy = date('Y-m-d');
	$mes = date('Y-m-d', strtotime('+1 month'));
	$licenciasvenc = Licencia::where('vencimiento', '<=', $hoy )->get();
	$licenciasxvencer = Licencia::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	$soatvenc = Seguro::where('vencimiento', '<=', $hoy)->get();
	$soatxvencer = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	foreach ($licenciasvenc as $licencia) {
		$lv++;
	}

	foreach ($licenciasxvencer as $licencia) {
		$lw++;
	}

	foreach ($soatvenc as $soat) {
		$sv++;
	}

	foreach ($soatxvencer as $soat) {
		$sw++;
	}

	$notificaciones = $lv + $lw + $sv + $sw;
	$hoy = date('Y-m-d', strtotime('-5 Hours'));
	$seguros = Seguro::where('vencimiento', '<=', $mes)->Where('vencimiento', '>', $hoy )->get();
	return View::make('seguros.vencidos')->with('seguros', $seguros)->with('notificaciones', $notificaciones)
	->with('lv', $lv)->with('lw', $lw)->with('sv', $sv)->with('sw', $sw);
});
/*
//Ruta para ingresar un administrador a la base de datos
Route::get('admin', function(){

	//ingresamos la direccion de la agencia
	$direccion = new Direccion;
	$direccion->lugar = 'JR. GENERAL PRADO Nº. 1001';
	$direccion->distrito = 'HUANUCO';
	$direccion->provincia = 'HUANUCO';
	$direccion->departamento = 'HUANUCO';
	$direccion->save();

	//Ingresamos una agencia a donde pertenecerá
	//la persona que será usuario.
	$agencia = new Agencia;
	$agencia->nombre = 'HUANUCO';
	$agencia->telefono = '062-525037';
	$agencia->direcciones_id = $direccion->id;
	$agencia->save();

	//ingresamos primero la persona
	//no es necesario guardar una dirección
	$persona = new Persona;
	$persona->dni = '12345678';
	$persona->nombre = 'TOCACHE';
	$persona->apellidos = 'EXPRESS';
	$persona->telefono = '062-525037';
	$persona->direcciones_id = $direccion->id;
	$persona->save();

	//Ahora asignamos a esta persona como
	//usuario.
	$usuario = new Usuario;
	$usuario->password = Hash::make('12345678');
	$usuario->email = '12345678';
	$usuario->personas_id = $persona->id;
	$usuario->agencias_id = $agencia->id;
	$usuario->tipo = 1;
	$usuario->save();


	return Redirect::to('/');
});